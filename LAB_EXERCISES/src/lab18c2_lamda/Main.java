package lab18c2_lamda;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<String, String> func = s -> {
            StringBuilder returnVal = new StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                if (i % 2 == 1) {
                    returnVal.append(s.charAt(i));
                }
            }

            return returnVal.toString();

        };

        System.out.println(func.apply("1234567890"));

        System.out.println(everySecondChar(func, "1234567890"));
    }

    public static String everySecondChar(Function<String, String>func1, String a){
        return func1.apply(a);
    }
}
