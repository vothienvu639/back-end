package lab13c3_unittest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class UtilitiesTest {
    private String source;
    private String expect;
    private  Utilities utilities;

    public UtilitiesTest(String source, String expect) {
        this.source = source;
        this.expect = expect;
    }

    @Before
    public void before(){
        utilities = new Utilities();
    }

    @Test (expected = ArithmeticException.class)
    public void converter() {
        int a = 10, b = 0;
        utilities.converter(a, b);
    }

    @Parameterized.Parameters(name = "{index}: removePairs({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"ABCDEFF", "ABCDEF"},
                {"AB88EFFG", "AB8EFG"},
                {"112233445566", "123456"},
                {"ZYZQQB", "ZYZQB"},
                {"A", "A"}
        });
    }

    @Test
    public void removePairs() {
        String actual = utilities.removePairs(source);
        Assert.assertEquals(expect, actual);
    }

}