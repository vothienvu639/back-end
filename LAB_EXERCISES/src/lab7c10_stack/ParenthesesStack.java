package lab7c10_stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

/**
 * De: https://www.hackerrank.com/challenges/java-stack/problem?isFullScreen=true
 */
public class ParenthesesStack {
    static final Map<Character, Character> parentheses = new HashMap<>(3);

    static {
        parentheses.put(']', '[');
        parentheses.put('}', '{');
        parentheses.put(')', '(');
    }

    public static void main(String[] args) throws FileNotFoundException {
        String projectDir = System.getProperty("user.dir");
        File text = new File(projectDir + "/src/lab7c10_stack/input01.txt");
        Scanner sc = new Scanner(text);

        while (sc.hasNext()) {
            String in = sc.next();
            System.out.println(validParentheses(in));
        }

        sc.close();
    }

    static boolean validParentheses(String in) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < in.length(); i++) {
            Character c = in.charAt(i);
            if (!stack.isEmpty() && parentheses.containsKey(c)) {
                Character openParentheses = stack.pop();
                if (openParentheses != parentheses.get(c)) {
                    return false;
                }
            } else {
                stack.push(c);
            }
        }
        return stack.isEmpty();
    }
}
