package lab11c1_tinhdahinh;

public class Mitsubishi extends Car{
    public Mitsubishi(int cylinders, String name) {
        super(cylinders, name);
    }


    @Override
    public String startEngine () {
        return "mitsubishi startEngine()";
    }

    @Override
    public String accelerate () {
        return "mitsubishi accelerate()";
    }

    @Override
    public String brake () {
        return "mitsubishi brake()";
    }


}
