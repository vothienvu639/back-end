package lab15c3_serialization_ghifile;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        FileService<Company> companyFileService = new FileService<>();
        String projectDir = System.getProperty("user.dir");
        String fileName = projectDir + "\\src\\lab15c3_serialization_ghifile\\company.dat";
        String file = "..\\company.dat";
        // đọc dữ liệu từ file
        List<Company> companies = companyFileService.readFile(fileName);
        System.out.println("List company in file: ");
        companyFileService.printList(companies);

        Company shoppe = new Company("Shoppe", "admin@shoppe.com", "0123456789", "5 Science Park Drive, Shopee Building", "118265", "Singapore");
        companies.add(shoppe);

        // ghi dữ liệu vào file
        companyFileService.writeFile(fileName, companies);

        companies = companyFileService.readFile(fileName);
        System.out.println("List company in file: ");
        companyFileService.printList(companies);

        try {
            int a = 5 / 1;
//                ghm
        } catch (ArithmeticException e) {
            System.out.println("Doesn't div by zero!");
        } catch (Exception ex) {
            System.out.println("Doesn't div by zero!");
        } finally {
            System.out.println("Finally block!");
        }

    }

}

