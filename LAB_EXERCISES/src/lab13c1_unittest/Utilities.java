package lab13c1_unittest;

public class Utilities {

    // Removes pairs of the same character that are next
    // to each other, by removing one occurrence of the character.
    // "ABBCDEEF" -> "ABCDEF"
    // "ABCBDEEF" -> "ABCBDEF" (the two B's aren't next to each other, so they
    // aren't removed)
    public String removePairs(String source) {

        // If length is less than 2, there won't be any pairs
        if (source.length() < 2) {
            return source;
        }

        StringBuilder sb = new StringBuilder();
        char[] string = source.toCharArray();

        for (int i = 0; i < string.length-1; i++) {
            if (string[i] != string[i +1]) {
                sb.append(string[i]);
                if (i == string.length-2){
                    sb.append(string[i+1]);
                }
            }
        }

        return sb.toString();
    }

}