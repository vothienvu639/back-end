package lab13c1_unittest;

import org.junit.Assert;
import org.junit.Test;

public class UtilitiesTest {

    @Test
    public void removePairs() {
        String source = "ABBCDEEF";
        Utilities utilities = new Utilities();
        String actual = utilities.removePairs(source);

        String expect = "ABCDEF";
        Assert.assertEquals(expect, actual);

        String source2 = "ABCBDEEF";
        String expect2 = "ABCBDEF";
        String actual2 = utilities.removePairs(source2);
        Assert.assertEquals(expect2, actual2);
    }
}