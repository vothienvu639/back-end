package lab17c3_reentrantlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {
    private double balance;
    private String accountNumber;

    private Lock lock;

    public BankAccount(double balance, String accountNumber) {
        this.balance = balance;
        this.accountNumber = accountNumber;
        this.lock = new ReentrantLock();
    }

    public void deposit(double amount, String threadNum) {
        lock.lock();
        try {
            balance += amount;
            System.out.println("nap tien " + threadNum + ": " + balance);
        } finally {
            lock.unlock();
        }

    }

    public void withdraw(double amount, String threadNum) {
        lock.lock();
        try {
            balance -= amount;
            System.out.println("rut tien " + threadNum + ": " + balance);
        } finally {
            lock.unlock();
        }

    }

}
