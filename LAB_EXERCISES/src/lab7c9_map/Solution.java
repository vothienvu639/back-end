package lab7c9_map;

//Complete this code or write your own from scratch

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * De: https://www.hackerrank.com/challenges/phone-book/problem?isFullScreen=true
 */
class Solution{
    public static void main(String []argh) throws FileNotFoundException {
        String projectDir = System.getProperty("user.dir");
        File text = new File(projectDir + "/src/lab7c9_map/input00.txt");
        Scanner in = new Scanner(text);
        int n=in.nextInt();
        in.nextLine();
        Map<String, Integer> phoneNumberMap = new HashMap<>();
        for(int i=0;i<n;i++)
        {
            String name=in.nextLine();
            int phone=in.nextInt();
            in.nextLine();
            phoneNumberMap.put(name, phone);
        }
        while(in.hasNext())
        {
            String s=in.nextLine();
            Integer phoneNumber = phoneNumberMap.get(s);
            if (phoneNumber == null) {
                System.out.println("Not found");
            } else {
                System.out.println(s + "=" + phoneNumber);
            }
        }
    }
}