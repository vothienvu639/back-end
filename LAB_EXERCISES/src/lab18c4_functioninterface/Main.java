package lab18c4_functioninterface;

public class Main {
    public static void main(String[] args) {
        // Câu hỏi: Có rất nhiều interface trong Java SDK,
        // đôi khi chúng ta có thể sử dụng một biểu thức lambda
        // thay vì tạo một instance mà phải implement interface đó.
        // Với một interface cho trước, làm thế nào để bạn có thể biết là
        // có thể sử dụng được biểu thức lambda cho interface đó hay không
        // Trả lời: interface phải là function interface,
        // nó có thể chỉ có 1 phương thức duy nhất phải được implement
        // Một function interface có thể chứa nhiều hơn 1 phương thức
        // nhưng tất cả các phương thức phải có các dèault implement
        // Tài liệu cho một interface sẽ cho biết nó có ở trong function interface hay ko


        // Câu hỏi: Chúng ta có thể sử dụng một biểu thức lambda
        // để biểu diễn một instance cho interface java.io.concurrent.Callable không?
        // Trả lơi: Callable interface chỉ có 1 phương thức phải đc triển khai phương thức call()
        // Vì vậy có thể sử dụng lamda cho nó


        // Câu hỏi: Interface java.util.Comparator có phải một function interface hay không?
        // Có. Mặc dù có hơn 10 phương thức nhưng chỉ có 1 phương thức được thực hiện là compare()
    }
}
