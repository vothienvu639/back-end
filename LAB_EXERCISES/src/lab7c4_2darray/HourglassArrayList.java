package lab7c4_2darray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * De: https://www.hackerrank.com/challenges/java-2d-array/problem?isFullScreen=true
 */
public class HourglassArrayList {
    public static void main(String[] args) throws IOException {
        String projectDir = System.getProperty("user.dir");
        FileReader text = new FileReader(projectDir + "\\src\\lab7c4_2darray\\input00.txt");
        BufferedReader bufferedReader = new BufferedReader(text);
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        List<List<Integer>> arr = new ArrayList<>();

        IntStream.range(0, 6).forEach(i -> {
            try {
                arr.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(Integer::parseInt)
                                .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();

        List<Integer> totalNumberHourglassList = new ArrayList<>();

        final int hourglassesNumber = 3;
        for(int row = 0; row <= arr.size() - hourglassesNumber; row++) {
            for(int col = 0; col <= arr.get(col).size() - hourglassesNumber; col++) {
                List<List<Integer>> hourglassesArr = getHourglass(arr, row, col, hourglassesNumber);
                int totalNumberHourglass = sumNumberHourglass(hourglassesArr);
                totalNumberHourglassList.add(totalNumberHourglass);
            }
        }
        System.out.print(totalNumberHourglassList.stream().max(Integer::compare).orElse(0));
    }

    public static List<List<Integer>> getHourglass(List<List<Integer>> initArr, int row, int col, int dimNumber) {
        List<List<Integer>> copyInitArr = copyArray(initArr);
        List<List<Integer>> hourglassesArr = new ArrayList<>();
        for(int i = 0; i < dimNumber; i++) {
            List<Integer> subArr = copyInitArr.get(row + i).subList(col, col + dimNumber);
            hourglassesArr.add(i, subArr);
        }
        hourglassesArr.get(1).set(0, 0);
        hourglassesArr.get(1).set(2, 0);
        return hourglassesArr;
    }

    private static List<List<Integer>> copyArray(List<List<Integer>> initArr){
        List<List<Integer>> copyInitArr = new ArrayList<>();
        for(int i = 0; i < initArr.size(); i++) {
            List<Integer> subList = new ArrayList<>();
            for(int j = 0; j < initArr.size(); j++) {
                subList.add(initArr.get(i).get(j));
            }
            copyInitArr.add(subList);
        }
        return copyInitArr;
    }

    public static int sumNumberHourglass(List<List<Integer>> hourglassesArr) {
        int result = 0;
        for(List<Integer> subArr: hourglassesArr) {
            result += subArr.stream().reduce(Integer::sum).orElse(result);
        }
        return result;
    }
}