package lab7c3_1darray;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/java-1d-array/problem?isFullScreen=true
 * Đề: Đây là một dạng game trên một mảng dữ liệu
 * Khi bắt đầu bạn sẽ đứng ở vị trí thứ trong trong mảng
 * Từ vị trí này, bạn có thể
 * - Di chuyển về phía sau nếu có tồn tại ô thứ i - 1 và ô đó có chứa số 0
 * - Di chuyển đến phía trước:
 *      + nếu ô thứ i + 1 có chứa số 0
 *      + nếu ô thứ i + leap có chứa số 0
 *      + nếu bạn đang đứng ở ô thứ n - 1
 *      + nếu i + leap >= độ dài mảng
 */
public class MoveGame {
    public static boolean canWin(int leap, int[] game) {
        // Return true if you can win the game; otherwise,
        int n = game.length;

        for(int i = 0; i < n; i++) {
            if(game[i] == 1) {
                return false;
            } else if(i + leap >= n - 1) {
                return true;
            } else if(game[i + leap] == 0) {
                i = i + leap - 1;
            } else if (game[i + 1] == 0) {
                game[i] = 1;
            } else if (i-1 >= 0 && game[i - 1] == 0) {
                i = i - 1;
            } else {
                return false;
            }
        }

        return false;
    }

    public static void main(String[] args) throws FileNotFoundException {
        String projectDir = System.getProperty("user.dir");
        File text = new File(projectDir + "\\src\\lab7c3_1darray\\input03.txt");
        Scanner scan = new Scanner(text);
//        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
        }
        scan.close();

//        int leap = 2;
//        int[] game = {0, 1, 0, 1, 0, 1}; // yes
//        int leap = 6;
//        int[] game = {0, 0, 1, 1, 0, 0, 1, 1, 0, 0}; // no
//        int leap = 3;
//        int[] game = {0, 0, 1, 1, 0, 0, 1, 1, 0, 0}; // yes
//        int leap = 4;
//        int[] game = {0, 1, 1, 0, 0, 1, 1, 0, 1}; // yes
//        int leap = 41;
//        int[] game = {0, 0, 0, 0, 0, 1, 0, 1, 0}; // yes
//        int leap = 8;
//        int[] game = {0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1}; // yes
//        int leap = 5;
//        int[] game = {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1}; // no
//        int leap = 65;
//        int[] game = {0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0}; // yes
//        int leap = 95;
//        int[] game = {0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1}; // yes
//        int leap = 21;
//        int[] game = {0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1}; // no
//        int leap = 52;
//        int[] game = {0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0 ,0, 0, 0, 1, 1 ,1 ,1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1 ,0 ,0 ,0, 1, 1, 1, 1, 0}; //no


//        System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
    }
}


//class Solution {
//
//    public static boolean canWin(int leap, int[] game) {
//        // Return true if you can win the game; otherwise, return false.
//        return canWin(leap, game,0);
//    }
//    public static boolean canWin(int leap,int[] game,int pos){
//
//        if(pos<0) return false;
//        if(game[pos]==1) return false;
//        if(pos+1>=game.length) return true;
//        if(pos+leap>=game.length) return true;
//        game[pos]=1; // To block moving forward -backward
//        return   canWin(leap,game,pos+leap) || canWin(leap,game,pos+1) || canWin(leap,game,pos-1) ;
//    }
//    public static void main(String[] args) {
//        Scanner scan = new Scanner(System.in);
//        int q = scan.nextInt();
//        while (q-- > 0) {
//            int n = scan.nextInt();
//            int leap = scan.nextInt();
//
//            int[] game = new int[n];
//            for (int i = 0; i < n; i++) {
//                game[i] = scan.nextInt();
//            }
//
//            System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
//        }
//        scan.close();
//    }
//}