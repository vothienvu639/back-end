package lab13c2_unittest;

import org.junit.Assert;
import org.junit.Test;

public class UtilitiesTest {

    @Test
    public void everyNthChar() {
        char[] a = {'h', 'e', 'l', 'l', 'o'};
        char[] b = {'e', 'l'};
        int n = 2;
        Utilities utilities = new Utilities();
        char[] actual = utilities.everyNthChar(a, n);
//        char[] ecpect =


    }


    @Test
    public void everNthChar_WhenNGreaterThanLength(){
        char [] a = {'h', 'e', 'l', 'l', 'o'};
        int n = 6;
        Utilities utilities = new Utilities();
        char [] actual = utilities.everyNthChar(a, n);
        char [] expect = a;

        Assert.assertEquals(expect.length, actual.length);
        Assert.assertArrayEquals(expect, actual);
    }

    @Test
    public void nullIfOddLength(){
        String a = "qwertt";
        Utilities utilities = new Utilities();
        String actual = utilities.nullIfOddLength(a);
        String expect = a;
        Assert.assertNotNull(actual);
        Assert.assertEquals(expect, actual);
    }

    @Test
    public  void nullIfOddLength_WhenReturnNull(){
        String a = "qwert";
        Utilities utilities = new Utilities();
        String actual = utilities.nullIfOddLength(a);
        Assert.assertNull(actual);
    }

    @Test
    public void converter(){
        int a = 10;
        int b = 5;
        Utilities utilities = new Utilities();
        int actual = utilities.converter(a, b);
        int expect = 300;
        Assert.assertEquals(expect, actual);
    }
}