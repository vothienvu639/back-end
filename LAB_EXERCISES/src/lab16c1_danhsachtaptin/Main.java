package lab16c1_danhsachtaptin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        String dir = "D:\\Workspace\\html-css\\f8-exercise\\xaydungweb";
        Set<String> fileSet = listFilesUsingDirectoryStream(dir);
        System.out.println(fileSet);

        // Đọc dữ liệu trong file
        File file = new File("D:\\Workspace\\java\\funix.exercise\\LAB_EXERCISES\\src\\lab16c1_danhsachtaptin\\file.txt");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNext()) {
            System.out.println(scanner.next());
        }
    }

    /**
     * Hàm này dùng để lấy ra danh sách file name của thư mục được truyển vào
     * @param dir
     * @return 1 danh sách file name ko trùng tên
     * @throws IOException
     */
    public static Set<String> listFilesUsingDirectoryStream(String dir) throws IOException {
        Set<String> fileSet = new HashSet<>();
        // newDirectoryStream dùng để lấy ra tất cả các phần tử trong thư mục mà mình truyền vào
        // thành 1 danh sách, mình cần lặp qua danh sách đó để lấy ra file name của từng phần tử
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir))) {
            for (Path path : stream) {
                // isDirectory dùng để kiểm tra xem path truyền vào có phải là đường dẫn đến thư mục hay ko
                if (!Files.isDirectory(path)) {
                    fileSet.add(path.getFileName()
                            .toString());
                }
            }
        }
        return fileSet;
    }
}
