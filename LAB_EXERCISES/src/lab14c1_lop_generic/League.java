package lab14c1_lop_generic;

import java.util.ArrayList;
import java.util.Collections;

public class League<T extends Team> {
    public String name;
    private ArrayList<T> league = new ArrayList<>();

    public League(String name) {
        this.name = name;
    }

    public void add(T d) {

        if (!league.contains(d)) {
            league.add(d);
        }

    }

    public void showLeagueTable() {
        Collections.sort(league);
        for (T t : league) {
            System.out.println(t.getName() + ":" + t.ranking());
        }
    }
}
