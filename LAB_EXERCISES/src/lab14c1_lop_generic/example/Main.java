package lab14c1_lop_generic.example;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        BangXepHang<HocSinh> bangXepHang = new BangXepHang<>("Bang xep hang thanh tich mon toan");

        HocSinh man = new HocSinh();
        man.setName("Man");
        man.setScore(8);
        bangXepHang.add(man);

        HocSinh vu = new HocSinh();
        vu.setScore(9);
        vu.setName("Vu");
        bangXepHang.add(vu);

//        bangXepHang.add(3);

        ArrayList<HocSinh> danhsachhocsinh = bangXepHang.getDanhSachDuLieu();
        System.out.println(danhsachhocsinh);

        BangXepHang<Integer> bangXepHang1 = new BangXepHang<>("Bang xep hang so");
        bangXepHang1.add(1);
        bangXepHang1.add(3);
        bangXepHang1.add(2);
//        bangXepHang1.add(vu);

        ArrayList<Integer> danhsachSo = bangXepHang1.getDanhSachDuLieu();
        System.out.println(danhsachSo);

    }
}
