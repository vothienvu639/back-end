package lab14c1_lop_generic.example;

import java.util.ArrayList;

public class BangXepHang<T> {
    private String tenBangXepHang;
    private ArrayList<T> danhSachDuLieu = new ArrayList<>();

    public BangXepHang(String tenBangXepHang) {
        this.tenBangXepHang = tenBangXepHang;
    }

    public void add(T t) {
        if(!danhSachDuLieu.contains(t)) {
            danhSachDuLieu.add(t);
        }
    }

    public String getTenBangXepHang() {
        return tenBangXepHang;
    }

    public ArrayList<T> getDanhSachDuLieu() {
        return danhSachDuLieu;
    }

}
