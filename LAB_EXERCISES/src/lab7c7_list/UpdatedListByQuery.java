package lab7c7_list;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.util.stream.Collectors.toList;

/**
 * De: https://www.hackerrank.com/challenges/java-list/problem?isFullScreen=true
 */
public class UpdatedListByQuery {
    public static void main(String[] args) throws FileNotFoundException {
        String projectDir = System.getProperty("user.dir");
        File text = new File(projectDir + "/src/lab7c7_list/input00.txt");
        Scanner scanner = new Scanner(text);
        int n = scanner.nextInt();
        scanner.nextLine();
        List<Integer> arrIntegerLines = getIntArrayList(scanner);
        int q = scanner.nextInt();
        scanner.nextLine();
        for(int i = 0; i < q; i++) {
            String modifyStatement = scanner.nextLine();
            if(modifyStatement.equalsIgnoreCase("insert")) {
                int indexAdd = scanner.nextInt();
                int valueAdd = scanner.nextInt();
                arrIntegerLines.add(indexAdd, valueAdd);
            } else if (modifyStatement.equalsIgnoreCase("delete")) {
                int indexDel = scanner.nextInt();
                arrIntegerLines.remove(indexDel);
            }
            if(scanner.hasNext()) {
                scanner.nextLine();
            }
        }
        for (Integer num: arrIntegerLines) {
            System.out.print(num + " ");
        }
    }

    private static List<Integer> getIntArrayList(Scanner scanner) {
        String[] dataArr = scanner.nextLine().replaceAll("\\s+$", "").split(" ");
        List<Integer> subArr = Arrays.stream(dataArr).map(Integer::parseInt).collect(toList());
        return new ArrayList<>(subArr);
    }
}