package lab17c4_trylock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {
    private double balance;
    private String accountNumber;

    private Lock lock;

    public BankAccount(double balance, String accountNumber) {
        this.balance = balance;
        this.accountNumber = accountNumber;
        this.lock = new ReentrantLock();
    }

    public void deposit(double amount, String threadNum) {
        try{
            if(lock.tryLock(1000, TimeUnit.MICROSECONDS)){
                try {
                    balance += amount;
                    System.out.println("nap tien " + threadNum + ": " + balance);
                } finally {
                    lock.unlock();
                }
            }else{
                System.out.println("Could not get lock");
            }
        }catch (InterruptedException in){
            in.printStackTrace();
        }
    }

    public void withdraw(double amount, String threadNum) {
        try{
            if(lock.tryLock(1000, TimeUnit.MICROSECONDS)){
                try {
                    balance -= amount;
                    System.out.println("rut tien " + threadNum + ": " + balance);
                } finally {
                    lock.unlock();
                }
            }else{
                System.out.println("Could not get lock");
            }
        }catch (InterruptedException in){
            in.printStackTrace();
        }




    }

}
