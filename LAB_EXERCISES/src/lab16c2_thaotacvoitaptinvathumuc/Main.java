package lab16c2_thaotacvoitaptinvathumuc;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        String projectDir = System.getProperty("user.dir");
        String sourceDir = projectDir + "\\src\\lab16c2_thaotacvoitaptinvathumuc\\2022";
        Set<String> fileSet = main.listFileUsingDirectoryStream(sourceDir);
        Map<String, List<String>> mapFile = main.sortFile(fileSet);
        main.createFolderAndMoveFile(mapFile, sourceDir);
    }

    public Set<String> listFileUsingDirectoryStream(String dir) throws IOException{
        Set<String> fileSet = new HashSet<>();
        try (DirectoryStream<Path>stream = Files.newDirectoryStream(Paths.get(dir))){
            for (Path path : stream){
                if (!Files.isDirectory(path)){
                    if(removeFileInvalid(path)) {
                        fileSet.add(path.getFileName().toString());
                    }
                }
            }
        }
        return fileSet;
    }

    public boolean checkFileNameValid(String fileName){
        String regex = "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])(.txt)$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(fileName);
        return m.matches();
    }

    public boolean removeFileInvalid(Path path) {
        if (!checkFileNameValid(path.getFileName().toString())){
            try{
                Files.delete(path);
                return true;
            }catch (IOException e){
                return false;
            }
        }
        // Hàm này chỉ trả về false nếu như xảy ra lỗi,
        // còn ngươc lại vẫn trả về true do tên file là hợp lệ không cần xóa
        return true;
    }

    public Map<String, List<String>> sortFile(Set<String> listFile){
        Map<String, List<String>> mapFile = new HashMap<>();
        for (String fileName :listFile){
            String folderName = fileName.substring(0, 7);
            if (!mapFile.containsKey(folderName)){
                List<String> fileNameList = listFile.stream().filter(a -> a.contains(folderName)).toList();
                mapFile.put(folderName, fileNameList);
            }
        }
        return mapFile;
    }

    public void createFolderAndMoveFile (Map<String, List<String>> mapFile, String parentDir){
        for (String newFolder : mapFile.keySet()){
            try {
                Path path = Paths.get(parentDir, newFolder);
                Files.createDirectory(path);
                System.out.println("Directory is created!");

                // Lấy ra danh sách các file thuộc folder hiện tại (newFolder)
                // ví dụ folder hiện lại là "2022-01" thì danh sách của folder này là ["2022-01-01", "2022-01-10"]
                List<String> listFileInFolder = mapFile.get(newFolder);
                for(String file: listFileInFolder) {
                    // Lấy ra đường dẫn cũ của file
                    Path oldFilePath = Paths.get(parentDir, file);

                    // Lấy ra đường dẫn mới của file
                    Path newFilePath = Paths.get(parentDir + "\\" + newFolder, file);

                    // Di chuyển file từ oldFilePath sang newFilePath
                    Files.move(oldFilePath, newFilePath);
                    System.out.println("Move file :" + oldFilePath + " ===>  :" + newFilePath);
                }
            } catch (IOException e) {
                System.err.println("Failed to create directory!" + e.getMessage());
            }
        }
    }
}
