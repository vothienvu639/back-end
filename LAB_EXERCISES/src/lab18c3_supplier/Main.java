package lab18c3_supplier;

import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        Supplier<String> iLoveJava = ()->"I Love Java";
        String supplierResult = iLoveJava.get();
        System.out.println(supplierResult);
    }
}
