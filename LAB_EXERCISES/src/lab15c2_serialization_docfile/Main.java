package lab15c2_serialization_docfile;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.List;

import static lab15c2_serialization_docfile.File.getWinner;
import static lab15c2_serialization_docfile.File.readDices;

public class Main {
    public static void main(String[] args) {
        try{
            String projectDir = System.getProperty("user.dir");
            List<Integer> bobDices = readDices(projectDir + "\\src\\lab15c2_serialization_docfile\\bob.txt");
            System.out.println(bobDices);

            List<Integer> aliceDices = readDices(projectDir + "\\src\\lab15c2_serialization_docfile\\alice.txt");
            System.out.println(aliceDices);

            String winner = getWinner(bobDices, aliceDices);
            if (winner == null){
                System.out.println("Two people have equal points");
            }else{
                System.out.println("The winner is " + winner);
            }

        }catch (InvalidDiceException | NumberRollDiceNotEqualException e){
            System.out.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println("The file does not exist: " + e.getMessage());
        } catch (InputMismatchException e) {
            System.out.println("The file contains non numeric data");
        }
    }
}
