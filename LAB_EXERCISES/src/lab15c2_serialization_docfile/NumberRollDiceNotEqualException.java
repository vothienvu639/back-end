package lab15c2_serialization_docfile;

public class NumberRollDiceNotEqualException extends RuntimeException{
    public NumberRollDiceNotEqualException(String message) {
        super(message);
    }
}
