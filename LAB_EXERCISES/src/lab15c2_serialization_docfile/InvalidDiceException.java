package lab15c2_serialization_docfile;

public class InvalidDiceException extends RuntimeException{
    public InvalidDiceException(String message) {
        super(message);
    }
}
