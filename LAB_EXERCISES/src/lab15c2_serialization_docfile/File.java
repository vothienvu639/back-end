package lab15c2_serialization_docfile;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class File {

    public static List<Integer> readDices(String fileName) throws FileNotFoundException {
        List<Integer> numberDices = new ArrayList<>();
        Scanner sc = null;
        try {
            sc = new Scanner(new FileReader(fileName));
            while (sc.hasNextLine()) {
                int number = sc.nextInt();
                if(number > 6 || number < 1) {
                    throw new InvalidDiceException("Gia tri xuc xac phai nam trong khoang tu 1 - 6.");
                }
                numberDices.add(number);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Khong tim thay duong dan cua file: " + fileName);
        } finally {
            if (sc != null) {
                sc.close();
            }
        }
        return numberDices;
    }

    public static Integer sumOfDice(List<Integer> dices) {
        int sum = 0;
        for (int i = 0; i < dices.size(); i++){
            sum += dices.get(i);
        }
        return sum;
    }

    public static String getWinner(List<Integer> bobDices, List<Integer> aliceDices){
        if (bobDices.size() != aliceDices.size()){
            throw new NumberRollDiceNotEqualException("So luot tung xuc sac k bang nhau");
        }
        Integer sumBob = sumOfDice(bobDices);
        Integer sumAlice = sumOfDice(aliceDices);

        if (sumBob == sumAlice){
            return null;
        }else if (sumBob > sumAlice){
            return "bob";
        }else {
            return "alice";
        }
    }
}
