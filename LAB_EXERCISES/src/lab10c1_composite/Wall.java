package lab10c1_composite;

public class Wall {
    private String direction;

    public Wall (String direction) {
        this.direction = direction;
    }

    public String getDirection () {
        return direction;
    }


}
