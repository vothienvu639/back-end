package lab10c1_composite;

public class Lamp {
    private String style;
    private boolean battery;
    private int globRating;

    public Lamp(String style, boolean battery, int globRating) {
        this.style = style;
        this.battery = battery;
        this.globRating = globRating;
    }

    public int getGlobRating () {
        return globRating;
    }

    public String getStyle () {
        return style;
    }

    public boolean isBattery () {
        return battery;
    }

    public void turnOn () {
        System.out.println("den dang duoc bat");
    }

}
