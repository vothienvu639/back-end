package lab7c11_hashset;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

/**
 * De: https://www.hackerrank.com/challenges/java-hashset/problem?isFullScreen=true
 */
public class CountHashSet {
    public static void main(String[] args) throws FileNotFoundException {
        String projectDir = System.getProperty("user.dir");
        File text = new File(projectDir + "/src/lab7c11_hashset/input01.txt");
        Scanner s = new Scanner(text);
        int t = s.nextInt();
        String[] pair_left = new String[t];
        String[] pair_right = new String[t];

        for (int i = 0; i < t; i++) {
            pair_left[i] = s.next();
            pair_right[i] = s.next();
        }

        // Write your code here
        HashSet<String> set = new HashSet<String>();
        for (int i = 0; i < pair_left.length; i++) {
            String temp = pair_left[i] + " " + pair_right[i];
            set.add(temp);
            System.out.println(set.size());
        }

    }
}