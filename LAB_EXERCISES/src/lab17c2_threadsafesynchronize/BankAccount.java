package lab17c2_threadsafesynchronize;

public class BankAccount {
    private double balance;
    private String accountNumber;

    public BankAccount(double balance, String accountNumber) {
        this.balance = balance;
        this.accountNumber = accountNumber;
    }

//    public void deposit(double amount, String threadNum){
//            balance += amount;
//            System.out.println("nap tien " + threadNum + ": " + balance);
//    }
//
//    public void withdraw(double amount, String threadNum){
//            balance -= amount;
//            System.out.println("rut tien " + threadNum + ": " + balance);
//    }

    public void deposit(double amount, String threadNum){
        synchronized (this){
            balance += amount;
            System.out.println("nap tien " + threadNum + ": " + balance);
        }
    }

    public void withdraw(double amount, String threadNum){
        synchronized (this){
            balance -= amount;
            System.out.println("rut tien " + threadNum + ": " + balance);
        }
    }
}
