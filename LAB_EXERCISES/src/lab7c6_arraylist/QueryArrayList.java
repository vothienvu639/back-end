package lab7c6_arraylist;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.util.stream.Collectors.toList;

/**
 * De: https://www.hackerrank.com/challenges/java-arraylist/problem?isFullScreen=true
 */
public class QueryArrayList {
    public static void main(String[] args) throws FileNotFoundException {
        String projectDir = System.getProperty("user.dir");
        File text = new File(projectDir + "/src/lab7c6_arraylist/input00.txt");
        Scanner scanner = new Scanner(text);
        int n = scanner.nextInt();
        scanner.nextLine();
        List<List<Integer>> arrIntegerLines = getIntDimArrayList(n, scanner);
        int d = scanner.nextInt();
        scanner.nextLine();
        List<List<Integer>> arrQueryLines = getIntDimArrayList(d, scanner);
        findElementByQueries(arrQueryLines, arrIntegerLines);
    }

    private static void findElementByQueries(List<List<Integer>> arrQueryLines, List<List<Integer>> arrIntegerLines) {
        for(List<Integer> queryLine: arrQueryLines) {
            try {
                System.out.println(arrIntegerLines.get(queryLine.get(0) - 1).get(queryLine.get(1)));
            } catch (Exception ex) {
                System.out.println("ERROR!");
            }
        }
    }

    private static List<List<Integer>> getIntDimArrayList(int size, Scanner scanner) {
        List<List<Integer>> arrIntegerLines = new ArrayList<>();
        for(int i = 0; i < size; i++) {
            String[] dataArr = scanner.nextLine().replaceAll("\\s+$", "").split(" ");
            List<Integer> subArr = Arrays.stream(dataArr).map(Integer::parseInt).collect(toList());
            arrIntegerLines.add(i, subArr);
        }
        return arrIntegerLines;
    }
}