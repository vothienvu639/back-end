package lab17c1_thread;

public class Main {

    public static void main(String[] args) {
        final BankAccount account = new BankAccount(1000, "111111");

        Thread trThread1 = new Thread(){
            public void run(){
                account.deposit(300, "thread 1");
                account.withdraw(50, "thread 1");
            }
        };
        trThread1.start();

        Thread trThread2 = new Thread(){
            public  void run(){
                account.deposit(203.75, "thread 2");
                account.withdraw(100, "thread 2");
            }
        };
        trThread2.start();


        Thread trThread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                account.deposit(300, "thread 3");
                account.withdraw(50, "thread 3");
            }
        });
        trThread3.start();

        Thread trThread4 = new Thread(new Runnable() {
            @Override
            public void run() {
                account.deposit(203.75, "thread 4");
                account.withdraw(100, "thread 4");
            }
        });
        trThread4.start();
    }
}
