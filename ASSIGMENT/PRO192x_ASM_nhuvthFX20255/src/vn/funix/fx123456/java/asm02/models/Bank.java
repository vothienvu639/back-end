package vn.funix.fx123456.java.asm02.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Bank {
    private String id;
    List<Customer> customers;

    public Bank() {
        this.customers = new ArrayList<>();
        this.id = String.valueOf(UUID.randomUUID());
    }


    /**
     * hàm thêm khách hàng mới nếu CCCD chưa tồn tại
     * @param newCustomer : thông tin khách hàng mới
     * @return : true/false
     */
    public boolean addCustomer(Customer newCustomer) {
        boolean flag = false;
        for (Customer customer : this.customers) {
            if (customer.getCustomerId().equals(newCustomer.getCustomerId())) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            this.customers.add(newCustomer);
            return true;
        }
        return false;
    }


    /**
     * hàm kiểm tra sự tồn tại của khách hàng thông qua số CCCD
     * @param customerId : số CCCD
     * @return :true/false
     */
    public boolean isCustomerExisted(String customerId) {
        for (Customer customer : this.customers) {
            if (customer.getCustomerId().equals(customerId)) {
                return true;
            }
        }
        return false;
    }



    public List<Customer> getCustomers() {
        return customers;
    }

    public String getId() {
        return id;
    }


    /**
     * hàm thêm mới tài khoản thông qua số CCCD và thông tin tài khoản cần thêm mới
     * @param customerId : số CCCD
     * @param account : thông tin tài khoản mới
     * @return : true/false
     */
    public boolean addAccount(String customerId, Account account) {
        for (Customer customer : this.customers) {
            if (customer.getCustomerId().equals(customerId)) {
                customer.addAccount(account);
                return true;
            }
        }
        return false;
    }
}
