package vn.funix.fx123456.java.asm02.models;

public class User {
    private String name;
    private String customerId;

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerId() {
        return customerId;
    }


    /**
     * hàm thiết lập CCCD cho người dùng
     * @param customerId : số CCCD
     * @throws Exception
     */
    public void setCustomerId(String customerId) throws Exception {
        if (checkId(customerId)) {
            this.customerId = customerId;
        } else {
            throw new Exception("So CCCD khong hop le");
        }

    }


    /**
     * hàm kiểm tra số CCCD có hợp lệ hay không
     * @param customerId : số CCCD
     * @return : true/false
     */
    private boolean checkId(String customerId) {
        boolean isNumber = customerId.chars().allMatch(Character::isDigit);
        if (customerId.length() == 12 && isNumber && checkBirthPlace(customerId)) {
            return true;
        }
        return false;
    }


    /**
     * ham kiểm tra 3 số đầu của CCCD có thuộc 1 trong 64 tỉnh thành của VN hay k
     * @param id : số CCCD
     * @return :true/false
     */
    private boolean checkBirthPlace(String id) {
        boolean isBirthPlace = false;
        String birthplaceNum = id.substring(0, 3);
        switch (birthplaceNum) {
            case "001":
            case "002":
            case "004":
            case "006":
            case "008":
            case "010":
            case "011":
            case "012":
            case "014":
            case "015":
            case "017":
            case "019":
            case "020":
            case "022":
            case "024":
            case "025":
            case "026":
            case "027":
            case "030":
            case "031":
            case "033":
            case "034":
            case "035":
            case "036":
            case "037":
            case "038":
            case "040":
            case "042":
            case "044":
            case "045":
            case "046":
            case "048":
            case "049":
            case "051":
            case "052":
            case "054":
            case "056":
            case "058":
            case "060":
            case "062":
            case "064":
            case "066":
            case "067":
            case "068":
            case "070":
            case "072":
            case "074":
            case "075":
            case "077":
            case "079":
            case "080":
            case "082":
            case "083":
            case "084":
            case "086":
            case "087":
            case "089":
            case "091":
            case "092":
            case "093":
            case "094":
            case "095":
            case "096":
                isBirthPlace = true;
                break;
            default:
                isBirthPlace = false;
                break;
        }
        return isBirthPlace;
    }
}
