package vn.funix.fx123456.java.asm02.models;

import java.util.ArrayList;
import java.util.List;

public class Customer extends User {
    private List<Account> accounts = new ArrayList<>();

    public List<Account> getAccounts() {
        return accounts;
    }

    /**
     * hàm kiểm tra khách hàng có phải premium hay k
     * @return : true/false
     */
    public boolean isCustomerPremium() {
        for (Account account : this.accounts) {
            if (account.isAccountPremium()) {
                return true;
            }
        }
        return false;
    }


    /**
     * hàm thêm tài khoản mới nếu tài khoản đó chưa tồn tại
     * @param newAccount : thông tin tài khoản mới
     * @return : true/false
     */
    public boolean addAccount(Account newAccount) {
        boolean flag = false;
        if (accounts != null) {
            for (Account account : this.accounts) {
                if (account.getAccountNumber().equals(newAccount.getAccountNumber())) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                this.accounts.add(newAccount);
                return true;
            }
        }
        return false;

    }


    /**
     * hàm tính tổng số dư tất cả tài khoảng của khách hàng
     * @return : tổng tài khoản
     */
    public double getBalance() {
        double answer = 0;
        for (Account account : this.accounts) {
            answer += account.getBalance();
        }
        return answer;
    }


    /**
     * hàm định dạng số dư theo dạng tiền tệ VN
     * @param money : số tiền
     * @return : số tiền sau khi định dạng
     * 50000.0 -> 50,000đ
     */
    public String formatCurrencyVN(double money) {
        String totalMoney = String.format("%,.0f", money) + "đ";
        return totalMoney;
    }


    /**
     * hiển thị thông tin khách hàng
     */
    public void displayInformation() {
        String customerId = super.getCustomerId();

        String customerName = super.getName();

        String customerType = "";
        if (this.isCustomerPremium()) {
            customerType = "Premium";
        } else {
            customerType = "Normal";
        }
        String totalBalance = this.formatCurrencyVN(this.getBalance());

        System.out.println(customerId + "   |       " + customerName + " | " + customerType + " | " + totalBalance);

        int count = 1;
        for (Account account : this.accounts) {
            System.out.println(count + "    " + account.getAccountNumber() + "    |                       " + formatCurrencyVN(account.getBalance()));
            count++;
        }

    }


}
