package vn.funix.fx123456.java.asm02;

import vn.funix.fx123456.java.asm02.models.Account;
import vn.funix.fx123456.java.asm02.models.Bank;
import vn.funix.fx123456.java.asm02.models.Customer;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Asm02 {
    private static final Bank bank = new Bank();
    static final String AUTHOR = "nhuvthFX20255";
    static final String VERSION = "v2.0.0";

    public static void main(String[] args) throws Exception {
        displayMenu();
        int number = 0;
        do {
            number = inputModuleNumber();
            switch (number) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    insertCustomer();
                    break;
                case 2:
                    insertAccountToCustomer();
                    break;
                case 3:
                    displayCustomerList();
                    break;
                case 4:
                    searchCustomerByCCCD();
                    break;
                case 5:
                    searchCustomerByName();
                    break;
                default:
                    break;
            }
        } while (number != 0);
    }

    /**
     * ham hien thi menu chuc nang
     */
    public static void displayMenu() {
        System.out.println("+----------+-------------------------+----------+");
        System.out.printf("| NGAN HANG SO | %s@%s           |\n", AUTHOR, VERSION);
        System.out.println("+----------+-------------------------+----------+");
        System.out.println(" 1. Them khach hang");
        System.out.println(" 2. Them tai khoan cho khach hang");
        System.out.println(" 3. Hien thi danh sach khach hang");
        System.out.println(" 4. Tim theo CCCD");
        System.out.println(" 5. Tim theo ten khach hang");
        System.out.println(" 0. Thoat");
        System.out.println("+----------+-------------------------+----------+");
    }


    /**
     * ham lựa chọn chức năng theo số ở menu
     * @return : number
     */
    public static int inputModuleNumber() {
        boolean flag = false;
        int number = 0;
        do {
            System.out.print("Chuc nang: ");
            try {
                Scanner sc = new Scanner(System.in);
                number = sc.nextInt();
                flag = false;
            } catch (Exception ex) {
                flag = true;
                System.out.println("Vui long nhap so cua chuc nang");
            }
        } while (flag);
        return number;
    }

//    public boolean isModuleNumber (int number){
//        if (number >= 0 && number <= 5){
//            return true;
//        }
//        return false;
//    }

    /**
     * Chức năng 1: Thêm khách hàng vào ngân hàng
     */

    public static void insertCustomer() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap ten khach hang: ");
        String name = sc.nextLine();
        String id = "";
        do {
            System.out.print("Nhap so CCCD: ");
            id = sc.nextLine();
            if (id.equalsIgnoreCase("NO")) {
                System.exit(0);
            }
            if (!validateCustomerId(id)) {
                System.out.println("So CCCD khong hop le.");
                System.out.println("Vui long nhap lai hoac 'No' de thoat ");
            }
        } while (!validateCustomerId(id));
        Customer customer = new Customer();
        customer.setCustomerId(id);
        customer.setName(name);
        boolean add = bank.addCustomer(customer);
        if (add) {
            System.out.println("Da them khach hang " + id + " vao danh sach");
        } else {
            System.out.println("Them khach hang khong thanh cong do " + id + " da ton tai");
        }

    }

    /**
     * ham kiem tra CCCD co hop le khong
     *
     * @param id : ma so CCCD cua khach hang
     * @return : tra ve gia tri true/false
     */
    public static boolean validateCustomerId(String id) {
        boolean isNumeric = id.chars().allMatch(Character::isDigit);
        if (id.length() == 12 && isNumeric) {
            return true;
        }
        return false;
    }

    /**
     * Chức năng 2: Thêm tài khoản cho khách hàng
     */
    public static void insertAccountToCustomer() {
        Scanner sc = new Scanner(System.in);
        String id = "";
        Customer customer = null;
        do {
            System.out.print("Nhap CCCD khach hang: ");
            id = sc.nextLine();
            customer = existCustomer(id);
            if (customer == null) {
                System.out.println("So CCCD chua ton tai tren he thong. Vui long nhap lai so CCCD da dang ki voi ngan hang");
            }
        } while (customer == null);
        String stk = "";
        do {
            System.out.print("Nhap ma STK gom 6 chu so: ");
            stk = sc.nextLine();
        } while (invalidAccountNumber(stk, customer));
        double balance = 0;
        do {
            System.out.print("Nhap so du: ");
            balance = sc.nextDouble();
            if (balance < 50_000) {
                System.out.println("So du tai khoan khong duoc nho hon 50,000. Vui long nhap lai");
            }
        } while (balance < 50_000);
        Account account = new Account();
        account.setAccountNumber(stk);
        account.setBalance(balance);
        boolean add = customer.addAccount(account);
        if (add) {
            System.out.println("Quy khach da them tai khoan thanh cong");
        } else {
            System.out.println("Them tai khoan khong thanh cong");
        }
    }


    /**
     * hàm tìm khách hàng của ngân hàng bằng số CCCD
     * @param id : số CCCD
     * @return : customer
     */
    public static Customer existCustomer(String id) {
        List<Customer> customers = bank.getCustomers();
        for (Customer customer : customers) {
            if (customer.getCustomerId().equals(id)) {
                return customer;
            }
        }
        return null;
    }


    /**
     * hàm kiểm tra tính không hợp lệ của stk
     *  @param stk : số tài khoản
     * @param customer : khách hàng cần kiểm tra stk
     * @return :true/false
     */
    public static boolean invalidAccountNumber(String stk, Customer customer) {
        boolean isNumber = stk.chars().allMatch(Character::isDigit);
        if (stk.length() == 6 && isNumber) {
            List<Account> accounts = customer.getAccounts();
            if (accounts == null || accounts.isEmpty()) {
                return false;
            }

            for (Customer customer2 : bank.getCustomers()){
                for (Account account : customer2.getAccounts()) {
                    if (account.getAccountNumber().equals(stk)) {
                        return true;
                    }
                }
            }

            return false;

        }
        return true;
    }

    /**
     * Chức năng 3: Hiển thị danh sách khách hàng
     */
    public static void displayCustomerList() {
        for (Customer customer : bank.getCustomers()) {
            customer.displayInformation();
        }
    }


    /**
     * Chức năng 4: Tìm theo CCCD khách hàng
     */
    public static void searchCustomerByCCCD() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap so CCCD: ");
        String id = sc.nextLine();
        Customer customer = existCustomer(id);
        if (customer != null) {
            customer.displayInformation();
        } else {
            System.out.println("So CCCD khong ton tai tren he thong");
        }
    }

    /**
     * Chức năng 5: Tìm theo tên khách hàng
     */
    public static void searchCustomerByName() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap ten khach hang: ");
        String name = sc.nextLine();
        for (Customer customer : bank.getCustomers()) {
            // matchFound tra ve true neu tim thay va tra ve false neu ko tim thay
            String myStr = customer.getName();
            String searchWord = name.toLowerCase();
            Pattern pattern = Pattern.compile("[" + searchWord + "]");
            Matcher matcher = pattern.matcher(myStr.toLowerCase());
            boolean matchFound = matcher.find();
            if (matchFound) {
                customer.displayInformation();
            } else {
                System.out.println("Khong tim thay ten khach hang");
            }
        }
    }


}
