package vn.funix.fx123456.java.asm02.models;

public class Account {
    private String accountNumber;
    private double balance;


    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }


    /**
     * hàm kiểm tra loai tai khoản có phải premium hay k
     * @return : true/false
     */
    public boolean isAccountPremium() {
        if (this.balance >= 10_000_000) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                '}';
    }
}
