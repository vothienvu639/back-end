package vn.funix.fx123456.java.asm01;

import java.util.Random;
import java.util.Scanner;

public class Asm01 {
    public static void main(String[] args) {
        printInitProject();
        int module = choseModule();
        handleModuleInit(module);
    }

    //    hàm in ra khởi tạo dự án
    public static void printInitProject(){
        System.out.println("+----------+-------------------------+----------+");
        System.out.println("| NGAN HANG SO | nhuvthFX20255@v1.0.0           |");
        System.out.println("+----------+-------------------------+----------+");
        System.out.println("| 1. Nhap CCCD                                  |");
        System.out.println("| 0. Thoat                                      |");
        System.out.println("+----------+-------------------------+----------+");
    }

    //    hàm chọn chức năng
    public static int choseModule(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Chuc nang: ");
        int a = sc.nextInt();
        return a;
    }

    //    hàm xử lý lựa chọn ở chức năng khởi tạo dự an
    public static void handleModuleInit(int number){
        if (number == 1 ){
            inputToken();
        } else if (number == 0) {
            System.exit(0);
        } else {
            System.out.println("Vui long nhap lai");
            int moduleNum = choseModule();
            handleModuleInit(moduleNum);
        }
    }

    private static String generateToken() {
        String alphaNumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        String result = "";
        for(int i = 0; i < 6; i++) {
            Random random = new Random();
            int indexNumber = random.nextInt((alphaNumeric.length() - 1 - 6) + 1) + 6;
            char ch = alphaNumeric.charAt(indexNumber);
            result += ch;
        }
        return result;
    }

    //    ham nhap ma xac thuc
    public static void inputToken(){
//        Random random = new Random();
//        int value = random.nextInt((999 - 100) + 1) + 100;
        String value = generateToken();
        System.out.println("Nhap ma xac thuc: " + value);
        Scanner sc = new Scanner(System.in);
//        int b = sc.nextInt();
        String b = sc.next();
//        if (b == value){
        if(b.equals(value)) {
            String id = inputId();
            handleId(id);
        }else {
            System.out.println("Ma xac thuc khong dung. Vui long thu lai.");
            inputToken();
        }
    }


    //    ham nhap so CCCD
    public static String inputId(){
        System.out.print("Vui long nhap so CCCD: ");
        Scanner sc = new Scanner(System.in);
        String c = sc.next();
        return c;
    }

    //    ham xu ly CCCD da nhap
    public static void handleId(String id){
        boolean isNumeric = id.chars().allMatch( Character::isDigit );
        if (id.length() == 12 && isNumeric){
            printMainModule();
            int module3 = choseModule();
            handleMainModule(module3,id);
        }else {
            System.out.print("So CCCD khong hop le. Vui long nhap lai hoac 'No' de thoat: ");
            Scanner sc = new Scanner(System.in);
            String d = sc.next();
            if (d.equalsIgnoreCase("NO")){
                System.exit(0);
            }else {
                handleId(d);
            }

        }
    }


    //    ham in ra lua chon sau khi nhap CCCD thanh cong
    public static void printMainModule(){
        System.out.println("    | 1. Kiem tra noi sinh");
        System.out.println("    | 2. Kiem tra tuoi, gioi tinh");
        System.out.println("    | 3. Kiem tra so ngau nhien");
        System.out.println("    | 0. Thoat");
    }


    //    ham xu ly lua chon cua mainmodule
    public static void handleMainModule(int number, String id){
        switch (number){
            case 1:
                String birthPlace = checkBirthPlace(id);
                System.out.println("Noi sinh: " + birthPlace);
                break;
            case 2:
                String ageSex = checkAgeSex(id);
                System.out.println("Gioi tinh: " + ageSex) ;
                break;
            case 3:
                System.out.println("So ngau nhien: " + randomNumber(id));
                break;
            case 0:
                System.exit(0);
            default:
                System.out.println("Ma chuc nang khong hop le. Vui long nhap lai.");
                int numberModule = choseModule();
                handleMainModule(numberModule, id);
                break;
        }
        int numberModule = choseModule();
        handleMainModule(numberModule, id);
    }

    //    ham so ngau nhien
    public static String randomNumber(String id){
        String randomNum = id.substring(6);
        return randomNum;
    }

    //    ham kiem tra tuoi gioi tinh
    public static String checkAgeSex(String id){
        String sex = "";
        String sexNum = id.substring(3,4);
        String age = "";
        String ageNum = id.substring(4,6);
        switch (sexNum){
            case "0":
                sex = "nam";
                age = "19" + ageNum;
                break;
            case "1":
                sex = "nu";
                age = "19" + ageNum;
                break;
            case "2":
                sex = "nam";
                age = "20" + ageNum;
                break;
            case "3":
                sex = "nu";
                age = "20" + ageNum;
                break;
            case "4":
                sex = "nam";
                age = "21" + ageNum;
                break;
            case "5":
                sex = "nu";
                age = "21" + ageNum;
                break;
            case "6":
                sex = "nam";
                age = "22" + ageNum;
                break;
            case "7":
                sex = "nu";
                age = "22" + ageNum;
                break;
            case "8":
                sex = "nam";
                age = "23" + ageNum;
                break;
            case "9":
                sex = "nu";
                age = "23" + ageNum;
                break;
            default:
                sex = "Ma gioi tinh khong hop le";
                break;
        }
        return sex + " | " + age;
    }

    //    ham kiem tra noi sinh
    public static String checkBirthPlace(String id){
        String birthPlace = "";
        String birthplaceNum = id.substring(0,3);
        switch (birthplaceNum){
            case "001":
                birthPlace = "HA NOI";
                break;
            case "002":
                birthPlace = "HA GIANG";
                break;
            case "004":
                birthPlace = "CAO BANG";
                break;
            case "006":
                birthPlace = "BAC KAN";
                break;
            case "008":
                birthPlace = "TUYEN QUANG";
                break;
            case "010":
                birthPlace = "LAO CAI";
                break;
            case "011":
                birthPlace = "DIEN BIEN";
                break;
            case "012":
                birthPlace = "LAI CHAU";
                break;
            case "014":
                birthPlace = "SON LA";
                break;
            case "015":
                birthPlace = "YEN BAI";
                break;
            case "017":
                birthPlace = "HOA BINH";
                break;
            case "019":
                birthPlace = "THAI NGUYEN";
                break;
            case "020":
                birthPlace = "LANG SON";
                break;
            case "022":
                birthPlace = "QUANG NINH";
                break;
            case "024":
                birthPlace = "BAC GIANG";
                break;
            case "025":
                birthPlace = "PHU THO";
                break;
            case "026":
                birthPlace = "VINH PHUC";
                break;
            case "027":
                birthPlace = "BAC NINH";
                break;
            case "030":
                birthPlace = "HAI DUONG";
                break;
            case "031":
                birthPlace = "HAI PHONG";
                break;
            case "033":
                birthPlace = "HUNG YEN";
                break;
            case "034":
                birthPlace = "THAI BINH";
                break;
            case "035":
                birthPlace = "HA NAM";
                break;
            case "036":
                birthPlace = "NAM DINH";
                break;
            case "037":
                birthPlace = "NINH BINH";
                break;
            case "038":
                birthPlace = "THANH HOA";
                break;
            case "040":
                birthPlace = "NGHE AN";
                break;
            case "042":
                birthPlace = "HA TINH";
                break;
            case "044":
                birthPlace = "QUANG BINH";
                break;
            case "045":
                birthPlace = "QUANG TRI";
                break;
            case "046":
                birthPlace = "THUA THIEN HUE";
                break;
            case "048":
                birthPlace = "DA NANG";
                break;
            case "049":
                birthPlace = "QUANG NAM";
                break;
            case "051":
                birthPlace = "QUANG NGAI";
                break;
            case "052":
                birthPlace = "BINH DINH";
                break;
            case "054":
                birthPlace = "PHU YEN";
                break;
            case "056":
                birthPlace = "KHANH HOA";
                break;
            case "058":
                birthPlace = "NINH THUAN";
                break;
            case "060":
                birthPlace = "BINH THUAN";
                break;
            case "062":
                birthPlace = "KON TUM";
                break;
            case "064":
                birthPlace = "GIA LAI";
                break;
            case "066":
                birthPlace = "DAK LAK";
                break;
            case "067":
                birthPlace = "DAK NONG";
                break;
            case "068":
                birthPlace = "LAM DONG";
                break;
            case "070":
                birthPlace = "BINH PHUOC";
                break;
            case "072":
                birthPlace = "TAY NINH";
                break;
            case "074":
                birthPlace = "BINH DUONG";
                break;
            case "075":
                birthPlace = "DONG NAI";
                break;
            case "077":
                birthPlace = "BA RIA - VUNG TAU";
                break;
            case "079":
                birthPlace = "HO CHI MINH";
                break;
            case "080":
                birthPlace = "LONG AN";
                break;
            case "082":
                birthPlace = "TIEN GIANG";
                break;
            case "083":
                birthPlace = "BEN TRE";
                break;
            case "084":
                birthPlace = "TRA VINH";
                break;
            case "086":
                birthPlace = "VINH LONG";
                break;
            case "087":
                birthPlace = "DONG THAP";
                break;
            case "089":
                birthPlace = "AN GIANG";
                break;
            case "091":
                birthPlace = "KIEN GIANG";
                break;
            case "092":
                birthPlace = "CAN THO";
                break;
            case "093":
                birthPlace = "HAU GIANG";
                break;
            case "094":
                birthPlace = "SOC TRANG";
                break;
            case "095":
                birthPlace = "BAC LIEU";
                break;
            case "096":
                birthPlace = "CA MAU";
                break;
            default:
                birthPlace = "Ma noi sinh khong ton tai";
                break;
        }
        return birthPlace;
    }


}
