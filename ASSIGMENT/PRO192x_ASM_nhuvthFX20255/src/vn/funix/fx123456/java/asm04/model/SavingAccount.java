package vn.funix.fx123456.java.asm04.model;

import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.dao.AccountDao;
import vn.funix.fx123456.java.asm04.dao.IReport;

import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

public class SavingAccount extends Account implements IReport, Serializable {
    @Serial
    private final static long serialVersionUID = 4341064044307281120L;
    public static final double SAVINGS_ACCOUNT_MAX_WITHDRAW = 5_000_000;
    public static final double SAVINGS_ACCOUNT_MIN_WITHDRAW = 50_000;


    /**
     * ham rut tien
     * @param amount so tien can rut
     * @return true-> rut tien thanh cong, false-> rut tien that bai
     * @throws IOException
     */
    public boolean withdraw(double amount) throws IOException {
        String time = Common.dateTimeString(LocalDateTime.now());

        if (isAccepted(amount)){
            createTransaction(amount, time, true, TransactionType.WITHDRAW.getValue());
            AccountDao.update(this);
            return true;
        }
        createTransaction(amount, time, false, TransactionType.WITHDRAW.getValue());
        return false;
    }

    /**
     * ham chuyen tien
     * @param receiveAccount tai khoan nhan
     * @param amount so tien chuyen/ nhan
     * @throws IOException
     */
    public void transfer (Account receiveAccount, double amount) throws IOException {
        String time = Common.dateTimeString(LocalDateTime.now());

        if (isAccepted(amount)){
            createTransaction(amount, time, true, TransactionType.TRANSFERS.getValue());
            receiveAccount.createTransaction(amount, time, true, TransactionType.DEPOSIT.getValue());
            AccountDao.update(this);
//            receiveAccount.setBalance(receiveAccount.getBalance() + amount);
            AccountDao.update(receiveAccount);
        }else {
            createTransaction(amount, time, false, TransactionType.TRANSFERS.getValue());
        }
    }

    /**
     * hàm kiểm tra tính hợp lệ của số tiền cần rút
     * @param amount : số tiền cần rút
     * @return: true/ false
     */
    public boolean isAccepted(double amount) {
        double remainBalance = super.getBalance()-amount;
        if (super.isAccountPremium() && amount >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && remainBalance >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && amount % 10_000 == 0 ){
            return true;
        }else if (amount >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && amount <= SAVINGS_ACCOUNT_MAX_WITHDRAW
                && remainBalance >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && amount % 10_000 == 0) {
            return true;
        }
        return false;
    }

    /**
     * hàm in ra biên lai giao dịch
     * @param amount : số tiền cần rút
     */
    @Override
    public void log(double amount, String type, String receiveAccount) {
        System.out.println("+----------+--------------------------+----------+");
        System.out.println("            BIEN LAI GIAO DICH SAVINGS");
        System.out.println();
        String time = Common.dateTimeString(LocalDateTime.now());
        System.out.printf("  Ngay G/D:                 %s%n", time);
        System.out.printf("  ATM ID:                 %s%n", "DIGITAL-BANK-ATM 2023");
        System.out.printf("  SO TK:                                 %s%n", getAccountNumber());
        if (receiveAccount != null) {
            System.out.printf("  SO TK NHAN:                             %s%n", receiveAccount);
            System.out.printf("  SO TIEN CHUYEN:                      %s%n", Common.formatCurrencyVN(amount));
        } else {
            System.out.printf("  SO TIEN:                           %s%n", Common.formatCurrencyVN(amount));
        }

        System.out.printf("  SO DU TK:                          %s%n", Common.formatCurrencyVN(getBalance()));
        System.out.println("  PHI + VAT:                                 0đ");
        System.out.println("+----------+--------------------------+----------+");
    }
}
