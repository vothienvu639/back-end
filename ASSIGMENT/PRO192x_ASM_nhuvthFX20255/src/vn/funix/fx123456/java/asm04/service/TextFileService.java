package vn.funix.fx123456.java.asm04.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFileService {
    private static final String COMMA_DELIMITED = ",";

    public static List<List<String>> readFile(String fileName){
        List<List<String>> dataList = new ArrayList<>();
        try{
            File myObj = new File(fileName);
            Scanner myReader = new Scanner(myObj);
            while(myReader.hasNextLine()){
                String data = myReader.nextLine();
                List<String> splitData = List.of(data.split(COMMA_DELIMITED));
                dataList.add(splitData);
            }
            myReader.close();
        }catch (FileNotFoundException e){
            System.out.println("khong tim thay file");
            e.printStackTrace();
        }
        return dataList;
    }

}
