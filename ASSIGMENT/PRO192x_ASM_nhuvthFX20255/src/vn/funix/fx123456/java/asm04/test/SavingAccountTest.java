package vn.funix.fx123456.java.asm04.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm03.models.DigitalBank;
import vn.funix.fx123456.java.asm03.models.DigitalCustomer;
import vn.funix.fx123456.java.asm03.models.SavingsAccount;

public class SavingAccountTest {
    @Test
    public void isAccepted_WhenIsPremium_ReturnTrue() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089777777777", "VU");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089777777777");

        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("121212");
        savingsAccount.setBalance(10_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        SavingsAccount savingsAccount1 = digitalCustomer.getSavingsAccounts().get(0);

        boolean actual = savingsAccount1.withdraw(100_000);

        Assert.assertTrue(actual);

    }


    @Test
    public void isAccepted_WhenIsNormal_ReturnTrue() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089777777777", "VU");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089777777777");

        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("121212");
        savingsAccount.setBalance(5_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        SavingsAccount savingsAccount1 = digitalCustomer.getSavingsAccounts().get(0);

        boolean actual = savingsAccount1.withdraw(100_000);

        Assert.assertTrue(actual);

    }

    @Test
    public void isAccepted_WhenReturnFalse() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089777777777", "VU");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089777777777");

        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("121212");
        savingsAccount.setBalance(4_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        SavingsAccount savingsAccount1 = digitalCustomer.getSavingsAccounts().get(0);

        boolean actual = savingsAccount1.withdraw(103_000);

        Assert.assertFalse(actual);

    }
}
