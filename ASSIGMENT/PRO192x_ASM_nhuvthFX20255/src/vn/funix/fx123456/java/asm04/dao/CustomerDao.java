package vn.funix.fx123456.java.asm04.dao;

import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.model.Customer;
import vn.funix.fx123456.java.asm04.service.BinaryFileService;

import java.io.IOException;
import java.util.List;

public class CustomerDao {
    private final static String FILE_PATH = Common.CUSTOMER_DAT_FILE_PATH;

    public static void save(List<Customer> customerList) throws IOException{
        BinaryFileService.writeFile(FILE_PATH ,  customerList);
    }

    public static List<Customer> list() throws IOException {
        return BinaryFileService.readFile(FILE_PATH);
    }

}
