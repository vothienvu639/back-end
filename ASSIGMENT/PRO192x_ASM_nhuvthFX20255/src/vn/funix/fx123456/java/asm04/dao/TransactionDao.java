package vn.funix.fx123456.java.asm04.dao;

import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.model.Transaction;
import vn.funix.fx123456.java.asm04.service.BinaryFileService;

import java.io.IOException;
import java.util.List;

public class TransactionDao {
    private final static String FILE_PATH = Common.TRANSACTION_DAT_FILE_PATH;

    public static void save(List<Transaction> transactionList) throws IOException {
        List<Transaction> existedTransactions = list();
        transactionList.addAll(existedTransactions);
        BinaryFileService.writeFile(FILE_PATH ,  transactionList);
    }

    public static List<Transaction> list() throws IOException {
        return BinaryFileService.readFile(FILE_PATH);
    }

}
