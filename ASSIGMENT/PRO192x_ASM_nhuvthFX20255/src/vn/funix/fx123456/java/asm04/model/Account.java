package vn.funix.fx123456.java.asm04.model;

import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.dao.AccountDao;
import vn.funix.fx123456.java.asm04.dao.CustomerDao;
import vn.funix.fx123456.java.asm04.dao.TransactionDao;

import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Account implements Serializable {
    @Serial
    private final static long serialVersionUID = 0;

    private String customerId;
    private String accountNumber;
    private double balance;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * lay thong tin giao dich thuoc tai khoan
     * @return list giao dich
     * @throws IOException
     */
    public List<Transaction> getTransaction() throws IOException {
        List<Transaction> transactions = TransactionDao.list();
        List<Transaction> transactionList = transactions.stream().filter(transaction -> transaction.getAccountNumber().equals(this.accountNumber)).toList();
        return transactionList;
    }

    /**
     * lay danh dach khach hang thuoc tai khoan
     * @return danh sach khach hang
     * @throws IOException
     */
    public Customer getCustomer() throws IOException {
        List<Customer> customers = CustomerDao.list();
        List<Customer> customerList = customers.stream().filter(customer -> customer.getCustomerId().equals(this.customerId)).toList();
        if (customerList.size() > 0){
            return customerList.get(0);
        }
        return null;
    }

    /**
     * hien thi danh sach giao dich
     * @throws IOException
     */
    public void displayTransactionList() throws IOException {
        List<Transaction> transactionList = getTransaction();
        for (Transaction transaction : transactionList){
            System.out.println("[GD] " + transaction.getAccountNumber() +" | " + transaction.getType() + " | " + Common.formatCurrencyVN(transaction.getAmount()) + " | " + transaction.getTime());
        }
    }

    /**
     * tao giao dich
     * @param amount so tien can giao dich
     * @param time thoi gian giao dich
     * @param status trang thai giao dich
     * @param type loai giao dich
     * @throws IOException
     */
    public void createTransaction(double amount, String time, boolean status, String type) throws IOException {
        Transaction transaction = new Transaction();
        if(type.equals(TransactionType.DEPOSIT.getValue())){
            transaction.setAmount(amount);
        }else {
            transaction.setAmount(-amount);
        }

        transaction.setStatus(status);
        transaction.setTime(time);
        transaction.setAccountNumber(accountNumber);
        transaction.setType(type);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        TransactionDao.save(transactions);

        if (status){
            if(type.equals(TransactionType.DEPOSIT.getValue())){
                this.setBalance(getBalance() + amount);
            }else {
                this.setBalance(getBalance() - amount);
            }
        }
    }

    /**
     * tao moi tai khoan
     * @param scanner nhap thong tin tai khoan
     * @throws IOException
     */
    public void input(Scanner scanner) throws IOException {
        String stk = Common.enterAccountNumber(scanner, null);
        double balance = Common.enterBalance(scanner);
        Account account = new SavingAccount();
        account.setAccountNumber(stk);
        account.setBalance(balance);
        account.setCustomerId(customerId);

        List<Account> accounts = new ArrayList<>();
        accounts.add(account);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        String time = formatter.format(LocalDateTime.now());
        try {
            AccountDao.save(accounts);
            createTransaction(balance, time, true, TransactionType.DEPOSIT.getValue());
        } catch (IOException e){
            createTransaction(balance, time, false, TransactionType.DEPOSIT.getValue());
        }
    }

    /**
     * hàm kiểm tra loai tai khoản có phải premium hay k
     * @return : true/false
     */
    public boolean isAccountPremium() {
        if (this.balance >= 10_000_000) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Double.compare(account.balance, balance) == 0 && Objects.equals(customerId, account.customerId) && Objects.equals(accountNumber, account.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, accountNumber, balance);
    }
}
