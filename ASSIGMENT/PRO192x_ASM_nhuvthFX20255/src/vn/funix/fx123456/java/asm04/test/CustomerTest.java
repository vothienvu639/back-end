package vn.funix.fx123456.java.asm04.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.dao.AccountDao;
import vn.funix.fx123456.java.asm04.exception.CustomerIdNotValidException;
import vn.funix.fx123456.java.asm04.model.Account;
import vn.funix.fx123456.java.asm04.model.Customer;
import vn.funix.fx123456.java.asm04.model.SavingAccount;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CustomerTest {
    private Customer customer;
    private Customer customer1;

    private List<Account> accounts;
    private List<Account> account1List;

    @Before
    public void test() throws CustomerIdNotValidException, IOException {
        customer = new Customer("089000000001", "vu");
        customer1 = new Customer("089010101010", "thien");

        accounts = new ArrayList<>();
        Account account = new SavingAccount();
        account.setCustomerId("089000000001");
        account.setAccountNumber("111112");
        account.setBalance(5_000_000);
        accounts.add(account);

        account1List = new ArrayList<>();
        Account account1 = new SavingAccount();
        account1.setCustomerId("089010101010");
        account1.setAccountNumber("222221");
        account1.setBalance(11_000_000);
        account1List.add(account1);
    }

    @Test
    public void getAccounts() throws IOException {
        try(MockedStatic<AccountDao> mockedStatic = Mockito.mockStatic(AccountDao.class)){
            mockedStatic.when(AccountDao::list).thenReturn(accounts);
            List<Account> accountList = customer.getAccounts();
            Assert.assertEquals(1, accountList.size());
            Assert.assertEquals(accounts.get(0).getCustomerId(), accountList.get(0).getCustomerId());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getAccountNotSuccess(){
        try (MockedStatic<AccountDao> mockedStatic = Mockito.mockStatic(AccountDao.class)){
            mockedStatic.when(AccountDao::list).thenReturn(accounts);
            List<Account> accountList = customer1.getAccounts();
            Assert.assertEquals(0, accountList.size());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getAccountByAccountNumber(){
        Account account = customer.getAccountByAccountNumber(accounts, "111112");
        Assert.assertNotNull(account);
        Assert.assertEquals("111112", account.getAccountNumber());
    }

    @Test
    public void getAccountByAccountNumberNull(){
        Account account1 = customer.getAccountByAccountNumber(accounts, "222222");
        Assert.assertNull(account1);
    }

    @Test
    public void getTotalAccountBalance() throws IOException {
        try(MockedStatic<AccountDao> mockedStatic = Mockito.mockStatic(AccountDao.class)) {
            mockedStatic.when(AccountDao::list).thenReturn(accounts);
            double actual = customer.getTotalAccountBalance();
            Assert.assertEquals(Common.formatCurrencyVN(5_000_000), Common.formatCurrencyVN(actual));
        }
    }

    @Test
    public void getTotalAccountBalanceNotSuccess(){
        try(MockedStatic<AccountDao> mockedStatic = Mockito.mockStatic(AccountDao.class)){
            mockedStatic.when(AccountDao::list).thenReturn(new ArrayList<>());
            double actual = customer.getTotalAccountBalance();
            Assert.assertEquals(Common.formatCurrencyVN(0), Common.formatCurrencyVN(actual));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void isCustomerNormal(){
        try(MockedStatic<AccountDao> mockedStatic = Mockito.mockStatic(AccountDao.class)){
            mockedStatic.when(AccountDao::list).thenReturn(accounts);
            boolean actual= customer.isCustomerPremium();
            Assert.assertFalse(actual);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void isCustomerPremium(){
        try(MockedStatic<AccountDao> mockedStatic = Mockito.mockStatic(AccountDao.class)){
            mockedStatic.when(AccountDao::list).thenReturn(account1List);
            boolean actual = customer1.isCustomerPremium();
            Assert.assertTrue(actual);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
