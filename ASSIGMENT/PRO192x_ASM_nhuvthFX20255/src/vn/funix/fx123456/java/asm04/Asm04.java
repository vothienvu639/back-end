package vn.funix.fx123456.java.asm04;

import vn.funix.fx123456.java.asm02.Asm02;
import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.dao.CustomerDao;
import vn.funix.fx123456.java.asm04.exception.CustomerIdNotValidException;
import vn.funix.fx123456.java.asm04.model.Customer;
import vn.funix.fx123456.java.asm04.model.DigitalBank;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Asm04 {
    static final String AUTHOR = "nhuvthFX20255";
    static final String VERSION = "v4.0.0";

    static DigitalBank digitalBank = new DigitalBank();

    public static void main(String[] args) throws CustomerIdNotValidException, IOException {
        Scanner scanner = new Scanner(System.in);
        while (true){
            displayMenu();
            int chucNang = inputModuleNumber();
            switch (chucNang){
                case 1:
                    digitalBank.showCustomers();
                    break;
                case 2:
                    enterCustomerList();
                    break;
                case 3:
                    addSavingAccount(scanner);
                    break;
                case 4:
                    transfer(scanner);
                    break;
                case 5:
                    withdraw(scanner);
                    break;
                case 6:
                    searchTransactionHistory(scanner);
                    break;
                case 0:
                    System.exit(0);
                default:
                    System.out.println("Vui long nhap so cua chuc nang");
                    break;
            }
        }
    }

    /**
     * tìm kiếm lịch sử giao dịch theo mã khách hàng
     * @param scanner: nhập mã khách hàng
     * @throws IOException
     */
    public static void searchTransactionHistory (Scanner scanner) throws IOException {
        System.out.println("Nhap ma so cua khach hang: ");
        String maSo = scanner.next();
        List<Customer> customerList = CustomerDao.list();
        Customer customer = digitalBank.getCustomerById(customerList, maSo);
        customer.displayTransactionInformation();
    }

    /**
     * hàm rút tiền
     * @param scanner: nhập mã số khách hàng
     * @throws IOException
     */
    public static void withdraw (Scanner scanner) throws IOException {
        System.out.println("Nhap ma so cua khach hang: ");
        String maSo = scanner.next();
        digitalBank.withdraw(scanner, maSo);
    }

    /**
     * hàm chuyển tiền
     * @param scanner: nhập mã số khách hàng
     * @throws IOException
     */
    public static void transfer (Scanner scanner) throws IOException {
        System.out.println("Nhap ma so cua khach hang: ");
        String maSo = scanner.next();
        digitalBank.transfer(scanner, maSo);
    }

    /**
     * hàm tạo taif khoản
     * @param scanner : nhập mã số khách hàng
     * @throws IOException
     */
    public static void addSavingAccount(Scanner scanner) throws IOException {
        System.out.println("Nhap ma so cua khach hang: ");
        String maSo = scanner.nextLine();
        digitalBank.addSavingAccount(scanner, maSo);
    }

    /**
     * hàm nhập danh sách khách hàng
     * @throws CustomerIdNotValidException
     * @throws IOException
     */
    public static void enterCustomerList() throws CustomerIdNotValidException, IOException {
        String fileName = Common.CUSTOMER_TXT_FILE_PATH;
        digitalBank.addCustomers(fileName);
    }

    /**
     * ham hien thi menu chuc nang
     */
    public static void displayMenu() {
        System.out.println();
        System.out.println();
        System.out.println("+----------+-------------------------+----------+");
        System.out.printf("| NGAN HANG SO | %s@%s           |\n", AUTHOR, VERSION);
        System.out.println("+----------+-------------------------+----------+");
        System.out.println(" 1. Xem danh sach khach hang");
        System.out.println(" 2. Nhap danh sach khach hang");
        System.out.println(" 3. Them tai khoan ATM");
        System.out.println(" 4. Chuyen tien");
        System.out.println(" 5. Rut tien");
        System.out.println(" 6. Tra cuu lich su giao dich");
        System.out.println(" 0. Thoat");
        System.out.println("+----------+-------------------------+----------+");
        System.out.println();
    }

    /**
     * ham lựa chọn chức năng theo số ở menu
     *
     * @return : number
     */
    public static int inputModuleNumber() {
        return Asm02.inputModuleNumber();
    }
}


