package vn.funix.fx123456.java.asm04.model;

import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.dao.AccountDao;
import vn.funix.fx123456.java.asm04.dao.CustomerDao;
import vn.funix.fx123456.java.asm04.exception.CustomerIdNotValidException;
import vn.funix.fx123456.java.asm04.service.TextFileService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DigitalBank extends Bank {

    /**
     * xem danh khach khach hang
     * @throws IOException
     */
    public void showCustomers() throws IOException {
        List<Customer> customers = CustomerDao.list();
        if (customers.size() == 0){
            System.out.println("Khong co khach hang nao trong danh sach");
            return;
        }
        System.out.println("+------------------------------------------------------+");
        System.out.println("|                XEM DANH SACH KHACH HANG              |");
        System.out.println("+------------------------------------------------------+");
        List<Account> accounts = AccountDao.list();
        for (Customer customer1 : customers){
            List<Account> listAccountByCustomerId = accounts.stream().filter(account -> account.getCustomerId().equals(customer1.getCustomerId())).toList();
            double totalBalance = listAccountByCustomerId.stream().mapToDouble(Account::getBalance).sum();
            String customerType = "";
            if (customer1.isCustomerPremium()){
                customerType = "Premium";
            }else {
                customerType = "Normal";
            }
            System.out.println("+------------------------------------------------------+");
            System.out.println(customer1.getCustomerId() + "   | " + customer1.getName() + "   | " + customerType + "   | " + Common.formatCurrencyVN(totalBalance));
            int count = 0;
            for(Account account : listAccountByCustomerId){
                count++;
                System.out.println(count + "     " + account.getAccountNumber() + "   |                     " + Common.formatCurrencyVN(account.getBalance()) );
            }
        }
    }

    /**
     * them khach hang
     * @param fileName ten khach hang
     * @throws CustomerIdNotValidException
     * @throws IOException
     */
    public void addCustomers(String fileName) throws CustomerIdNotValidException, IOException {
        List<List<String>> customerList = TextFileService.readFile(fileName);
        List<Customer> customers = new ArrayList<>();
        for (List<String> customer: customerList){
            if(Common.checkId(customer.get(0))){
                List<Customer> listExistedCustomer = CustomerDao.list();
                long countExistedCustomer = listExistedCustomer.stream().filter(existedCustomer -> existedCustomer.getCustomerId().equals(customer.get(0))).count();
                if (countExistedCustomer == 0 ) {
                    Customer customer1 = new Customer(customer.get(0), customer.get(1));
                    customers.add(customer1);
                }else {
                    System.out.println("Ma khach hang " + customer.get(0) + "da ton tai");
                }
            }else {
                System.out.println("Ma khach hang " + customer.get(0) + " khong hop le");

            }
        }

        if (customers.size()>0){
            try{
                List<Customer> customers1 = CustomerDao.list();
                customers.addAll(customers1);
                CustomerDao.save(customers);
                System.out.println("Nhap khach hang thanh cong");
            } catch (Exception e) {
                System.out.println("Nhap khach hang khong thanh cong");
            }
        }

    }

    /**
     * them tai khoan atm
     * @param scanner nhap lieu
     * @param customerId ma KH
     * @throws IOException
     */
    public void addSavingAccount(Scanner scanner, String customerId) throws IOException {
        List<Customer> customers = CustomerDao.list();
        Customer customerById = getCustomerById(customers, customerId);
        if (customerById == null){
            System.out.println("Khach hang chua ton tai");
            return;
        }
        String stk = Common.enterAccountNumber(scanner, customerById);
        double balance = Common.enterBalance(scanner);
        Account savingsAccount = new SavingAccount();
        savingsAccount.setCustomerId(customerId);
        savingsAccount.setAccountNumber(stk);
        savingsAccount.setBalance(balance);

        try{
            AccountDao.update(savingsAccount);
            System.out.println("Them tai khoan thanh cong");
        }catch (IOException e){
            System.out.println("Them tai khoan khong thanh cong");
        }
    }

    /**
     * ham rut tien
     * @param scanner nhap lieu
     * @param customerId ma khach hang
     * @throws IOException
     */
    public void withdraw (Scanner scanner, String customerId) throws IOException {
        List<Customer> customers = CustomerDao.list();
        Customer customer = getCustomerById(customers, customerId);
        if (customer == null){
            System.out.println("Khach hang khong ton tai");
            return;
        }
        List<Account> accountList = AccountDao.list();
        List<Account> accountByCustomerIdList = accountList.stream().filter(account -> account.getCustomerId().equals(customerId)).toList();

        double total = accountByCustomerIdList.stream().mapToDouble(Account::getBalance).sum();

        String customerType = "";
        if (customer.isCustomerPremium()){
            customerType = "Premium";
        }else {
            customerType = "Normal";
        }

        System.out.println(customer.getCustomerId() + " | " + customer.getName() + "        | " + customerType + Common.formatCurrencyVN(total));
        int count = 1;
        for (Account account : accountByCustomerIdList){
            System.out.println(count + "  " + account.getAccountNumber() + " | " + "SAVINGS            | " + "           " +Common.formatCurrencyVN(account.getBalance()));
            count++;
        }

        String stk = Common.enterAccountNumber(scanner, null);
        List<Account> accounts = accountByCustomerIdList.stream().filter(account -> account.getAccountNumber().equals(stk)).toList();
        SavingAccount account = new SavingAccount();
        if (accounts.size() >0){
            account = (SavingAccount) accounts.get(0);
        }else {
            System.out.println("So tai khoan khong ton tai");
            return;
        }
        double amount = 0;
        do {
            System.out.println("Nhap so tien can rut: ");
            amount = scanner.nextDouble();
            scanner.nextLine();
            if (!account.isAccepted(amount)){
                System.out.println("Vui long nhap lai so tien can rut");
            }
        }while (!account.isAccepted(amount));
        customer.withdraw(stk, amount);
    }

    /**
     * ham chueyn tien
     * @param scanner nhap lieu
     * @param customerId ma khach hang
     * @throws IOException
     */
    public void transfer(Scanner scanner, String customerId) throws IOException {
        List<Customer> customers = CustomerDao.list();
        Customer customer = getCustomerById(customers, customerId);
        if (customer == null){
            System.out.println("Khach hang khong ton tai");
            return;
        }

        // lấy ra danh sách tài khoản từ file accounts.dat
        List<Account> accountList = AccountDao.list();
        // tìm danh sách tài khoản theo customerId
        List<Account> accountByCustomerIdList = accountList.stream().filter(account -> account.getCustomerId().equals(customerId)).toList();

        double total = accountByCustomerIdList.stream().mapToDouble(Account::getBalance).sum();

        String customerType = "";
        if (customer.isCustomerPremium()){
            customerType = "Premium";
        }else {
            customerType = "Normal";
        }

        System.out.println(customer.getCustomerId() + " | " + customer.getName() + "        | " + customerType + Common.formatCurrencyVN(total));
        int count = 1;
        for (Account account : accountByCustomerIdList){
            System.out.println(count + "  " + account.getAccountNumber() + " | " + "SAVINGS            | " + "           " + Common.formatCurrencyVN(account.getBalance()));
            count++;
        }

        try{
            customer.transfers(scanner);
            System.out.println("Chuyen tien thanh cong");
        }catch (IOException e){
            System.out.println("Chuyen tien khong thanh cong");
        }
    }

    /**
     * kiem tra tai khoan co ton tai hay k
     * @param accountList danh sach tai khoan
     * @param newAccount tai khoan moi
     * @return true -> tk ton tai, false -> tk k ton tai
     */
    public boolean isAccountExisted (List<Account> accountList, Account newAccount){
//        if (accountList.contains(newAccount)){
//            return true;
//        }
//        return false;
        boolean matched = accountList.stream().anyMatch(account -> account.equals(newAccount));
        return matched;
    }

    /**
     * ham kiem tra khach hang co ton tai hay k
     * @param customers danh sach khach hang
     * @param newCustomer khach hang moi
     * @return true-> KH ton tai, false -> KH k ton tai
     */
    public boolean isCustomerExisted(List<Customer> customers, Customer newCustomer){
//        if (customers.contains(newCustomer)){
//            return true;
//        }
//        return false;
        boolean matched = customers.stream().anyMatch(customer -> customer.equals(newCustomer));
        return matched;
    }

    /**
     * tim thong tin KH theo ma KH hang
     * @param customerList ds khach hang
     * @param customerId ma KH
     * @return KH da tim dc hoac null
     */
    public Customer getCustomerById(List<Customer> customerList, String customerId){
        List<Customer> customerIds = customerList.stream().filter(customer -> customer.getCustomerId().equals(customerId)).toList();
        if (customerIds.size() > 0){
            return customerIds.get(0);
        }
        return null;
    }


}
