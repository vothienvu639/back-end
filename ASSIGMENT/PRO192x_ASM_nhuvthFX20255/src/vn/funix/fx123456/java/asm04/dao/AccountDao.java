package vn.funix.fx123456.java.asm04.dao;

import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.model.Account;
import vn.funix.fx123456.java.asm04.service.BinaryFileService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AccountDao {
    private final static String FILE_PATH = Common.ACCOUNT_DAT_FILE_PATH;
    private final static int MAX_THREAD = 5;

    public static void save(List<Account> accountList) throws IOException {
        BinaryFileService.writeFile(FILE_PATH ,  accountList);
    }

    public static List<Account> list() throws IOException {
        return BinaryFileService.readFile(FILE_PATH);
    }

    public static void update(Account editAccount) throws IOException {
        ExecutorService executorService = null;
        List<Account> existedAccounts = list();
        boolean hasExist = existedAccounts.stream().anyMatch(account -> account.getAccountNumber().equals(editAccount.getAccountNumber()));
        if(!hasExist) {
            existedAccounts.add(editAccount);
        } else {
            try {
                executorService = Executors.newFixedThreadPool(MAX_THREAD);
                Runnable runnableTask = () -> {
                    for (int i = 0; i < existedAccounts.size(); i++) {
                        if (existedAccounts.get(i).getAccountNumber().equals(editAccount.getAccountNumber())) {
                            existedAccounts.set(i, editAccount);
                        }
                    }
                };
                executorService.execute(runnableTask);
            } finally {
                if(executorService != null) {
                    executorService.shutdown();
                }
            }
        }

        save(existedAccounts);
    }

//    public static void update(Account editAccount) throws IOException {
//
//        List<Account> existedAccounts = list();
//        boolean hasExist = existedAccounts.stream().anyMatch(account -> account.getAccountNumber().equals(editAccount.getAccountNumber()));
//        if(!hasExist) {
//            existedAccounts.add(editAccount);
//        } else {
//            for (int i = 0; i < existedAccounts.size(); i++) {
//                if (existedAccounts.get(i).getAccountNumber().equals(editAccount.getAccountNumber())) {
//                    existedAccounts.set(i, editAccount);
//                }
//            }
//        }
//
//        save(existedAccounts);
//    }

}
