package vn.funix.fx123456.java.asm04.model;

import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.dao.AccountDao;
import vn.funix.fx123456.java.asm04.dao.CustomerDao;
import vn.funix.fx123456.java.asm04.exception.CustomerIdNotValidException;

import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Customer implements Serializable {
    @Serial
    private static final long serialVersionUID = 768;
    private String customerId;
    private String name;

    public String getCustomerId() {
        return customerId;
    }

    public String getName() {
        return name;
    }

    public Customer(String customerId, String name) throws CustomerIdNotValidException {
        this.name = name;
        if (Common.checkId(customerId)) {
            this.customerId = customerId;
        } else {
            throw new CustomerIdNotValidException("So CCCD khong hop le");
        }
    }

    public Customer(List<String> values) throws CustomerIdNotValidException {
        this(values.get(0), values.get(1));
    }

    /**
     * lay list tai khoan
     * @return list tai khoan
     * @throws IOException
     */
    public List<Account> getAccounts() throws IOException {
        List<Account> accounts = AccountDao.list();
        List<Account> accountList = accounts.stream().filter(account -> account.getCustomerId().equals(customerId)).toList();
        return accountList;
    }

    /**
     * lay thong tin tai khoan dua vao so tai khoan
     * @param accounts tai khoan
     * @param accountNumber so tai khoan
     * @return tai khoan
     */
    public Account getAccountByAccountNumber(List<Account> accounts, String accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }
        }
        return null;
    }

    /**
     * hien thi thong tin giao dich
     * @throws IOException
     */
    public void displayTransactionInformation() throws IOException {
        displayInformation();
        List<Account> accounts = getAccounts();
        for (Account account : accounts) {
            account.displayTransactionList();
        }

    }

    public void transfers(Scanner scanner) throws IOException {
        List<Account> accountByCustomerIdList = getAccounts();
        String stkChuyen = "";
        SavingAccount accountTransfer = null;
        do{
            System.out.println("Nhap so tai khoan: ");
            stkChuyen = scanner.next();
            accountTransfer = (SavingAccount) getAccountByAccountNumber(accountByCustomerIdList, stkChuyen);
            if (accountTransfer == null){
                System.out.println("So tai khoan khong ton tai, vui long nhap lai");
            }
        }while (accountTransfer == null);

        List<Account> allAccountList = AccountDao.list();
        String stkNhan = "";
        SavingAccount accountReceive = new SavingAccount();
        do{
            System.out.println("Nhap so tai khoan nhan: ");
            stkNhan = scanner.next();
            accountReceive = (SavingAccount) getAccountByAccountNumber(allAccountList, stkNhan);
            if (accountReceive == null){
                System.out.println("So tai khoan khong ton tai, vui long nhap lai");
            }
        }while (accountReceive == null);

        List<Customer> customers = CustomerDao.list();
        SavingAccount finalAccountReceive = accountReceive;
        List<Customer> customerList = customers.stream().filter(customer -> customer.getCustomerId().equals(finalAccountReceive.getCustomerId())).toList();
        if (!customerList.isEmpty()){
            Customer customer = customerList.get(0);
            System.out.println(stkNhan + " | " + customer.getName());

        }
        double amount = 0;
        do{
            System.out.println("Nhap so tien chuyen: ");
            amount = scanner.nextDouble();
            if (!accountTransfer.isAccepted(amount)){
                System.out.println("Vui long nhap lai so tien chuyen");
            }
        }while (!accountTransfer.isAccepted(amount));
        System.out.println("Xac nhan thuc hien chuyen " + Common.formatCurrencyVN(amount) + " tu tai khoan ["  + stkChuyen + "] den tai khoan [" + stkNhan + "] (Y/N): ");
        String xacNhan = scanner.next();
        if (xacNhan.equalsIgnoreCase("y")){
            accountTransfer.transfer(accountReceive, amount);
            System.out.println("Chuyen tien thanh cong, bien lai giao dich: ");
            accountTransfer.log(amount, TransactionType.TRANSFERS.getValue(), stkNhan);
        }else {
            System.out.println("Giao dich that bai");
        }
    }


    /**
     * hiển thị thông tin khách hàng
     */
    public void displayInformation() throws IOException {

        String customerType = "";
        if (this.isCustomerPremium()) {
            customerType = "Premium";
        } else {
            customerType = "Normal";
        }
        String totalBalance = Common.formatCurrencyVN(this.getTotalAccountBalance());

        System.out.println();
        System.out.println("+-------------+----------------------------------+-------------+");
        System.out.println("|                       LICH SU GIAO DICH                      |");
        System.out.println("+-------------+----------------------------------+-------------+");
        System.out.println(this.customerId + "   | " + this.name + "   | " + customerType + "   | " + totalBalance);

        int count = 1;

        List<Account> accounts = getAccounts();
        for (Account account : accounts) {
            System.out.println(count + "     " + account.getAccountNumber() + "   | SAVINGS   |        "
                    + Common.formatCurrencyVN(account.getBalance()));
            count++;
        }

    }

    /**
     * hàm tính tổng số dư tất cả tài khoảng của khách hàng
     *
     * @return : tổng tài khoản
     */
    public double getTotalAccountBalance() throws IOException {
        double answer = 0;
        for (Account account : getAccounts()) {
            answer += account.getBalance();
        }
        return answer;
    }


    public void withdraw(String stk, double amount) throws IOException {
        List<Account> accountList = getAccounts();
        if (accountList.isEmpty()) {
            System.out.println("Khach hang khong co tai khoan nao, thao tac khong thanh cong");
        } else {
            Account account = getAccountByAccountNumber(accountList, stk);
            SavingAccount savingAccount = (SavingAccount) account;

            try {
                if (savingAccount != null) {
                    boolean result = savingAccount.withdraw(amount);
                    if (result){
                        System.out.println("Rut tien thanh cong, bien lai giao dich: ");
                        savingAccount.log(amount, null, null);
                    }else {
                        System.out.println("Rut tien khong thanh cong");
                    }
                }
            } catch (IOException e) {
                System.out.println("Da co loi xay ra. Rut tien khong thanh cong");
            }
        }
    }

    /**
     * hàm kiểm tra khách hàng có phải premium hay k
     *
     * @return : true/false
     */
    public boolean isCustomerPremium() throws IOException {
        for (Account account : getAccounts()) {
            if (account.isAccountPremium()) {
                return true;
            }
        }

        return false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(customerId, customer.customerId) && Objects.equals(name, customer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, name);
    }
}

