package vn.funix.fx123456.java.asm04.exception;

public class CustomerIdNotValidException extends Throwable {
    private String message;

    public CustomerIdNotValidException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
