package vn.funix.fx123456.java.asm04.common;

import vn.funix.fx123456.java.asm04.model.Account;
import vn.funix.fx123456.java.asm04.model.Customer;
import vn.funix.fx123456.java.asm04.model.SavingAccount;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Common {

    public static String CUSTOMER_DAT_FILE_PATH = "D:\\Workspace\\java\\funix.exercise\\PRO192x_ASM_nhuvthFX20255\\PRO192x_ASM_nhuvthFX20255\\src\\vn\\funix\\fx123456\\java\\asm04\\store\\customers.dat";
    public static String CUSTOMER_TXT_FILE_PATH = "D:\\Workspace\\java\\funix.exercise\\PRO192x_ASM_nhuvthFX20255\\PRO192x_ASM_nhuvthFX20255\\src\\vn\\funix\\fx123456\\java\\asm04\\store\\customers.txt";
    public static String ACCOUNT_DAT_FILE_PATH = "D:\\Workspace\\java\\funix.exercise\\PRO192x_ASM_nhuvthFX20255\\PRO192x_ASM_nhuvthFX20255\\src\\vn\\funix\\fx123456\\java\\asm04\\store\\accounts.dat";
    public static String TRANSACTION_DAT_FILE_PATH = "D:\\Workspace\\java\\funix.exercise\\PRO192x_ASM_nhuvthFX20255\\PRO192x_ASM_nhuvthFX20255\\src\\vn\\funix\\fx123456\\java\\asm04\\store\\transactions.dat";

    /**
     * chuyển dữ liệu từ ngày tháng sang kiểu chuỗi
     * @param dateTime: ngày tháng cần chuyển đổi
     * @return ngày tháng đã được chuyển đổi thành chuỗi
     */
    public static String dateTimeString (LocalDateTime dateTime){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        String time = dateTime.format(dateTimeFormatter);
        return time;
    }

    /**
     * hảmf xử lý quá trình nhập số dư
     * @param scanner : nhập số dư
     * @return tra ve so du
     */
    public static double enterBalance (Scanner scanner){
        double balance = 0;
        do {
            System.out.println("Nhap so du tai khoan >= 50_000d: ");
            balance = scanner.nextDouble();
            scanner.nextLine();
            if (balance < 50_000){
                System.out.println("So du khong hop le. Vui long nhap lai");
            }
        }while(balance < 50_000);
        return balance;
    }

    /**
     * ham nhap so tai khoan
     * @param scanner nhap so tai khoan
     * @param customerById thong tin khach hang
     * @return so tai khoan
     * @throws IOException
     */
    public static String enterAccountNumber (Scanner scanner, Customer customerById) throws IOException {
        String stk = "";
        do{
            System.out.println("Nhap so tai khoan gom 6 chu so: ");
            stk = scanner.next();
            if (validateAccount(stk)){
                System.out.println("So tai khoan khong hop le. Vui long nhap lai");
            }else if (customerById != null && isAccountNumberExisted(stk, customerById)){
                System.out.println("So tai khoan da ton tai. Vui long long nhap lai");
            }
        } while(validateAccount(stk) || (customerById != null && isAccountNumberExisted(stk, customerById)));
        return stk;
    }

    /**
     * hàm kiểm tra stk đã tồn tại chưa. Tồn tại => true, không tồn tại => false
     * @param stk : số tài khoản
     * @return true/false
     */
    public static boolean isAccountNumberExisted(String stk, Customer customer) throws IOException {
        for(Account savingsAccount : customer.getAccounts()){
            if (savingsAccount.getAccountNumber().equals(stk)){
                return true;
            }
        }
        return false;
    }

    /**
     * hàm kiểm tra tính không hợp lệ của stk
     *  @param stk : số tài khoản
     * @return :true/false
     */
    public static boolean validateAccount(String stk) {
        boolean isNumber = stk.chars().allMatch(Character::isDigit);
        return stk.length() != 6 || !isNumber;
    }

    /**
     * hàm định dạng số dư theo dạng tiền tệ VN
     * @param money : số tiền
     * @return : số tiền sau khi định dạng
     * 50000.0 -> 50,000đ
     */
    public static String formatCurrencyVN(double money) {
        String totalMoney = String.format("%,.2f", money) + "đ";
        return totalMoney;
    }

    /**
     * hàm kiểm tra số CCCD có hợp lệ hay không
     * @param customerId : số CCCD
     * @return : true/false
     */
    public static boolean checkId(String customerId) {
        boolean isNumber = customerId.chars().allMatch(Character::isDigit);
        if (customerId.length() == 12 && isNumber && checkBirthPlace(customerId)) {
            return true;
        }
        return false;
    }


    /**
     * ham kiểm tra 3 số đầu của CCCD có thuộc 1 trong 64 tỉnh thành của VN hay k
     * @param id : số CCCD
     * @return :true/false
     */
    public static boolean checkBirthPlace(String id) {
        boolean isBirthPlace = false;
        String birthplaceNum = id.substring(0, 3);
        switch (birthplaceNum) {
            case "001":
            case "002":
            case "004":
            case "006":
            case "008":
            case "010":
            case "011":
            case "012":
            case "014":
            case "015":
            case "017":
            case "019":
            case "020":
            case "022":
            case "024":
            case "025":
            case "026":
            case "027":
            case "030":
            case "031":
            case "033":
            case "034":
            case "035":
            case "036":
            case "037":
            case "038":
            case "040":
            case "042":
            case "044":
            case "045":
            case "046":
            case "048":
            case "049":
            case "051":
            case "052":
            case "054":
            case "056":
            case "058":
            case "060":
            case "062":
            case "064":
            case "066":
            case "067":
            case "068":
            case "070":
            case "072":
            case "074":
            case "075":
            case "077":
            case "079":
            case "080":
            case "082":
            case "083":
            case "084":
            case "086":
            case "087":
            case "089":
            case "091":
            case "092":
            case "093":
            case "094":
            case "095":
            case "096":
                isBirthPlace = true;
                break;
            default:
                break;
        }
        return isBirthPlace;
    }

}
