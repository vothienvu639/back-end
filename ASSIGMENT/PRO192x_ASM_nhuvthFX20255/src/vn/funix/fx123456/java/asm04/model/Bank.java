package vn.funix.fx123456.java.asm04.model;

import java.util.UUID;

public class Bank {
    private final String bankId = String.valueOf(UUID.randomUUID());;
    private String bankName;

    public String getBankId() {
        return bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
