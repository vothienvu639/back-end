package vn.funix.fx123456.java.asm04.dao;

public interface IReport {
    void log(double amount, String type, String receiveAccount);
}
