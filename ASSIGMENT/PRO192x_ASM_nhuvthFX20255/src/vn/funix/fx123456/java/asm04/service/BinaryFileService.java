package vn.funix.fx123456.java.asm04.service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BinaryFileService {

    public static <T> List<T> readFile(String fileName) throws IOException {
//        System.out.println(fileName);
        List<T> objects = new ArrayList<>();
        try (ObjectInputStream file = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fileName)))) {
            boolean eof = false;
            while (!eof) {
                try {
                    T object = (T) file.readObject();
                    objects.add(object);
                } catch (EOFException e) {
                    eof = true;
                }
            }
        } catch (FileNotFoundException fn) {
            System.out.println("Khong tim thay file");
        } catch (EOFException e) {
            return new ArrayList<>();
        } catch (IOException io) {
            System.out.println("IO Exception: " + io.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException: " + e.getMessage());
        }
        return objects;
    }


    public static <T> void writeFile(String fileName, List<T> objects) throws IOException {
        try (ObjectOutputStream file = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
            for (T object : objects) {
                file.writeObject(object);
            }
        } catch (FileNotFoundException fn) {
            System.out.println("Khong tim thay file");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
