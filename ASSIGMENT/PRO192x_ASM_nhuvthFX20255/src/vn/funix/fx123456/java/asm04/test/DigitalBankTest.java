package vn.funix.fx123456.java.asm04.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import vn.funix.fx123456.java.asm04.exception.CustomerIdNotValidException;
import vn.funix.fx123456.java.asm04.model.Account;
import vn.funix.fx123456.java.asm04.model.Customer;
import vn.funix.fx123456.java.asm04.model.DigitalBank;
import vn.funix.fx123456.java.asm04.model.SavingAccount;

import java.util.ArrayList;
import java.util.List;

public class DigitalBankTest {
    private DigitalBank digitalBank;
    private List<Account> accounts;
    private List<Customer> customers;

    @Before
    public void test() throws CustomerIdNotValidException {

        digitalBank = new DigitalBank();
        accounts = new ArrayList<>();
        Account account = new SavingAccount();
        account.setAccountNumber("111111");
        account.setCustomerId("089000000000");
        account.setBalance(15_000_000);
        accounts.add(account);

        customers = new ArrayList<>();
        Customer customer = new Customer("089111111111", "man");
        customers.add(customer);
    }

    @Test
    public void isAccountExisted(){
        Account account1 = new SavingAccount();
        account1.setAccountNumber("111111");
        account1.setCustomerId("089000000000");
        account1.setBalance(15_000_000);
        boolean actual = digitalBank.isAccountExisted(accounts, account1);
        Assert.assertTrue(actual);
    }

    @Test
    public void isAccountExistedFalse(){
        Account account1 = new SavingAccount();
        account1.setAccountNumber("111112");
        account1.setCustomerId("089000000000");
        account1.setBalance(15_000_000);
        boolean actual = digitalBank.isAccountExisted(accounts, account1);
        Assert.assertFalse(actual);
    }

    @Test
    public void isCustomerExisted() throws CustomerIdNotValidException {
        Customer customer1 = new Customer("089111111111", "man");

        boolean actual = digitalBank.isCustomerExisted(customers, customer1);
        Assert.assertTrue(actual);
    }

    @Test
    public void isCustomerExistedFalse() throws CustomerIdNotValidException {
        Customer customer1 = new Customer("089111111112", "man");

        boolean actual = digitalBank.isCustomerExisted(customers, customer1);
        Assert.assertFalse(actual);
    }

    @Test
    public void getCustomerById(){
        Customer actual = digitalBank.getCustomerById(customers, "089111111111");
        Assert.assertNotNull(actual);
        Assert.assertEquals(customers.get(0).getCustomerId(), actual.getCustomerId());
    }
}
