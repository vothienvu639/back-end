package vn.funix.fx123456.java.asm04.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import vn.funix.fx123456.java.asm04.common.Common;
import vn.funix.fx123456.java.asm04.dao.CustomerDao;
import vn.funix.fx123456.java.asm04.dao.TransactionDao;
import vn.funix.fx123456.java.asm04.exception.CustomerIdNotValidException;
import vn.funix.fx123456.java.asm04.model.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class AccountTest {

    private Account account;

    private List<Transaction> transactions;

    private List<Customer> customers;

    @Before
    public void setUp() throws CustomerIdNotValidException {
        account = new SavingAccount();

        transactions = new ArrayList<>();
        Transaction transaction = new Transaction();
        transaction.setType(TransactionType.DEPOSIT.getValue());
        transaction.setTime(Common.dateTimeString(LocalDateTime.now()));
        transaction.setAccountNumber("111111");
        transaction.setAmount(200_000);
        transaction.setStatus(true);
        transactions.add(transaction);

        Transaction transaction1 = new Transaction();
        transaction1.setAccountNumber("222222");
        transaction1.setStatus(false);
        transaction1.setType(TransactionType.DEPOSIT.getValue());
        transaction1.setAmount(300_000);
        transactions.add(transaction1);

        customers = new ArrayList<>();

        Customer customer = new Customer("089000000000", "vu");
        customers.add(customer);

    }


    @Test
    public void getTransaction(){
        try(MockedStatic<TransactionDao> mockedStatic = Mockito.mockStatic(TransactionDao.class)) {
            mockedStatic.when(TransactionDao::list).thenReturn(transactions);
            account.setAccountNumber("111111");
            List<Transaction> transactionList = account.getTransaction();
            Assert.assertEquals(1, transactionList.size());
            Assert.assertEquals(transactions.get(0).getAccountNumber(), transactionList.get(0).getAccountNumber());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void getTransactionNotFindListData(){
        try(MockedStatic<TransactionDao> mockedStatic = Mockito.mockStatic(TransactionDao.class)){
            mockedStatic.when(TransactionDao::list).thenReturn(transactions);
            account.setAccountNumber("333333");
            List<Transaction> transactionList1 = account.getTransaction();
            Assert.assertEquals(0, transactionList1.size());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getCustomer(){
        try(MockedStatic<CustomerDao> mockedStatic = Mockito.mockStatic(CustomerDao.class)){
            mockedStatic.when(CustomerDao::list).thenReturn(customers);
            account.setCustomerId("089000000000");
            Customer customer = account.getCustomer();
            Assert.assertNotNull(customer);
            Assert.assertEquals(customers.get(0).getCustomerId(), customer.getCustomerId());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getCustomerNull(){
        try(MockedStatic<CustomerDao> mockedStatic = Mockito.mockStatic(CustomerDao.class)){
            mockedStatic.when(CustomerDao::list).thenReturn(customers);
            account.setCustomerId("089090909090");
            Customer customer1 = account.getCustomer();
            Assert.assertNull(customer1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void isAccountPremium(){
        account.setBalance(15_000_000);
        boolean actual = account.isAccountPremium();
        Assert.assertTrue(actual);
    }

    @Test
    public void isAccountNormal(){
        account.setBalance(5_000_000);
        boolean actual = account.isAccountPremium();
        Assert.assertFalse(actual);
    }
}
