package vn.funix.fx123456.java.asm04.dao;

import vn.funix.fx123456.java.asm04.model.Account;

public interface ITransfer {
    void transfer(Account receiveAccount, double amount);

}
