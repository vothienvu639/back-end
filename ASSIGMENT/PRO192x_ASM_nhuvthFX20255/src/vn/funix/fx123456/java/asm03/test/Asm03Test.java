package vn.funix.fx123456.java.asm03.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm03.Asm03;
import vn.funix.fx123456.java.asm03.models.DigitalCustomer;
import vn.funix.fx123456.java.asm03.models.LoansAccount;
import vn.funix.fx123456.java.asm03.models.SavingsAccount;

import static org.junit.Assert.*;

public class Asm03Test {

    @Test
    public void validateAccount_WhenAccountNumberLengthIsNotSix() {
        String stk = "1234567";
        boolean actual = Asm03.validateAccount(stk);

        Assert.assertTrue(actual);
    }

    @Test
    public void validateAccount_WhenAccountNumberLengthIsNotDigit() {
        String stk = "qwerty";
        boolean actual = Asm03.validateAccount(stk);

        Assert.assertTrue(actual);
    }

    @Test
    public void validateAccount_WhenReturnFalse() {
        String stk = "121212";
        boolean actual = Asm03.validateAccount(stk);

        Assert.assertFalse(actual);
    }

    @Test
    public void isAccountNumberExisted_WhenSavingsAccount() throws Exception {
        Asm03.activeBank.addCustomer(Asm03.CUSTOMER_ID, "Nhu");
        DigitalCustomer digitalCustomer = Asm03.activeBank.getCustomerById(Asm03.CUSTOMER_ID);
        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("121212");

        digitalCustomer.addSavingsAccount(savingsAccount);

        boolean actual = Asm03.isAccountNumberExisted("121212");

        Assert.assertTrue(actual);
    }

    @Test
    public void isAccountNumberExisted_WhenLoansAccount() throws Exception {
        Asm03.activeBank.addCustomer(Asm03.CUSTOMER_ID, "Nhu");
        DigitalCustomer digitalCustomer = Asm03.activeBank.getCustomerById(Asm03.CUSTOMER_ID);
        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("121213");

        digitalCustomer.addLoansAccount(loansAccount);

        boolean actual = Asm03.isAccountNumberExisted("121213");

        Assert.assertTrue(actual);
    }

    @Test
    public void isAccountNumberExisted_WhenReturnFalse() throws Exception {
        Asm03.activeBank.addCustomer(Asm03.CUSTOMER_ID, "Nhu");

        boolean actual = Asm03.isAccountNumberExisted("121215");

        Assert.assertFalse(actual);
    }


    @Test
    public void getAccountByAccountNumber_WhenSavingsAccount() throws Exception {
        Asm03.activeBank.addCustomer(Asm03.CUSTOMER_ID, "Nhu");
        DigitalCustomer digitalCustomer = Asm03.activeBank.getCustomerById(Asm03.CUSTOMER_ID);
        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("121613");
        savingsAccount.setBalance(5_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        SavingsAccount actual = (SavingsAccount) Asm03.getAccountByAccountNumber("121613");

        Assert.assertNotNull(actual);
        Assert.assertEquals("121613", actual.getAccountNumber());
        Assert.assertEquals(5_000_000, actual.getBalance(), 0);
    }

    @Test
    public void getAccountByAccountNumber_WhenLoansAccount() throws Exception {
        Asm03.activeBank.addCustomer(Asm03.CUSTOMER_ID, "Nhu");
        DigitalCustomer digitalCustomer = Asm03.activeBank.getCustomerById(Asm03.CUSTOMER_ID);
        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("821212");
        loansAccount.setBalance(5_000_000);

        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount actual = (LoansAccount) Asm03.getAccountByAccountNumber("821212");

        Assert.assertNotNull(actual);
        Assert.assertEquals("821212", actual.getAccountNumber());
        Assert.assertEquals(5_000_000, actual.getBalance(), 0);
    }

    @Test
    public void getAccountByAccountNumber_WhenReturnNull() throws Exception {
        Asm03.activeBank.addCustomer(Asm03.CUSTOMER_ID, "Nhu");

        LoansAccount actual = (LoansAccount) Asm03.getAccountByAccountNumber("191212");

        Assert.assertNull(actual);
    }

}