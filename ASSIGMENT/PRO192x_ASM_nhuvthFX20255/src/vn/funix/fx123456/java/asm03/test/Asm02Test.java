package vn.funix.fx123456.java.asm03.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm02.Asm02;

import static org.junit.Assert.*;

public class Asm02Test {

    @Test
    public void validateCustomerId_WhenReturnTrue() {
        String cccd = "089666666666";
        boolean actual = Asm02.validateCustomerId(cccd);

        Assert.assertTrue(actual);
    }

    @Test
    public void validateCustomerId_WhenReturnFalse() {
        String cccd = "0896666666662";
        boolean actual = Asm02.validateCustomerId(cccd);

        Assert.assertFalse(actual);

        String id = "qwertyuio";
        boolean actual2 = Asm02.validateCustomerId(id);

        Assert.assertFalse(actual2);
    }
}