package vn.funix.fx123456.java.asm03.models;

public interface Withdraw {

    // xu ly nghiep vu rut tien
    boolean withdraw(double amount);

    // xac dinh xem gia tri co thoa dieu kien rut tien hay k
    boolean isAccepted(double amount);


}
