package vn.funix.fx123456.java.asm03;

import vn.funix.fx123456.java.asm02.Asm02;
import vn.funix.fx123456.java.asm02.models.Account;
import vn.funix.fx123456.java.asm03.models.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Asm03 {
    static final String AUTHOR = "nhuvthFX20255";
    static final String VERSION = "v3.0.0";

    private static final int EXIT_COMMAND_CODE = 0;
    private static final int EXIT_ERROR_CODE = -1;
    private static final Scanner sc = new Scanner(System.in);
    public static final DigitalBank activeBank = new DigitalBank();
    public static final String CUSTOMER_ID = "001215000001";
    private static final String CUSTOMER_NAME = "FUNIX";
    //hạn mức tối đa
    private static final double LOAN_ACCOUNT_MAX_BALANCE = 100_000_000;


    public static void main(String[] args) throws Exception {

        activeBank.addCustomer(CUSTOMER_ID, CUSTOMER_NAME);

        int number = 0;
        do {
            displayMenu();
            number = inputModuleNumber();
            switch (number) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    showCustomer();
                    break;
                case 2:
                    addAccount();
                    break;
                case 3:
                    addAccountCredit();
                    break;
                case 4:
                    withdrawMoney();
                    break;
                case 5:
                    getTransactionHistory();
                    break;
                default:
                    break;
            }
        } while (true);
    }


    /**
     * ham hien thi menu chuc nang
     */
    public static void displayMenu() {
        System.out.println();
        System.out.println();
        System.out.println("+----------+-------------------------+----------+");
        System.out.printf("| NGAN HANG SO | %s@%s           |\n", AUTHOR, VERSION);
        System.out.println("+----------+-------------------------+----------+");
        System.out.println(" 1. Thong tin khach hang");
        System.out.println(" 2. Them tai khoan ATM");
        System.out.println(" 3. Them tai khoan tin dung");
        System.out.println(" 4. Rut tien");
        System.out.println(" 5. Lich su giao dich");
        System.out.println(" 0. Thoat");
        System.out.println("+----------+-------------------------+----------+");
        System.out.println();
    }


    /**
     * ham lựa chọn chức năng theo số ở menu
     *
     * @return : number
     */
    public static int inputModuleNumber() {
        return Asm02.inputModuleNumber();
    }


    /**
     * ham chuc nang 1: thông tin khách hàng
     */
    private static void showCustomer() {
        DigitalCustomer customer = activeBank.getCustomerById(CUSTOMER_ID);
        if (customer != null) {
            customer.displayInformation();
        }
    }


    /**
     * ham chuc nang 2: thêm tài khoản ATM
     */
    public static void addAccount(){
        DigitalCustomer customer = activeBank.getCustomerById(CUSTOMER_ID);;
        String stk = "";
        do {
            System.out.print("Nhap ma STK gom 6 chu so: ");
            stk = sc.nextLine();
            if(validateAccount(stk)) {
                System.out.println("So tai khoan KHONG HOP LE. Vui long nhap lai");
            } else if (isAccountNumberExisted(stk)){
                System.out.println("So tai khoan DA TON TAI. Vui long nhap lai");
            }
        } while (validateAccount(stk) || isAccountNumberExisted(stk));

        double balance = 0;
        do {
            System.out.print("Nhap so du: ");
            balance = sc.nextDouble();
            sc.nextLine();
            if (balance < 50_000) {
                System.out.println("So du tai khoan KHONG DUOC nho hon 50,000. Vui long nhap lai");
            }
        } while (balance < 50_000);

        SavingsAccount account = new SavingsAccount();
        account.setAccountNumber(stk);
        account.setBalance(balance);
        boolean add = customer.addSavingsAccount(account);

        Transaction transaction = new Transaction();
        transaction.setAccountNumber(stk);
        transaction.setStatus(add);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        transaction.setTime(LocalDateTime.now().format(dateTimeFormatter));
        transaction.setAmount(balance);
        account.getTransactions().add(transaction);

        if (add) {
            System.out.println("Quy khach da them tai khoan THANH CONG");
        } else {
            System.out.println("Them tai khoan KHONG thanh cong");
        }
    }


    /**
     * ham chuc nang 3: Thêm tài khoản tín dụng
     */
    public static void addAccountCredit(){

        DigitalCustomer customer = activeBank.getCustomerById(CUSTOMER_ID);;

        String stk = "";
        do {
            System.out.print("Nhap ma STK gom 6 chu so: ");
            stk = sc.nextLine();
            if(validateAccount(stk)) {
                System.out.println("So tai khoan KHONG HOP LE. Vui long nhap lai");
            } else if (isAccountNumberExisted(stk)){
                System.out.println("So tai khoan DA TON TAI. Vui long nhap lai");
            }
        } while (validateAccount(stk) || isAccountNumberExisted(stk));

        LoansAccount account = new LoansAccount();
        account.setAccountNumber(stk);
        account.setBalance(LOAN_ACCOUNT_MAX_BALANCE);
        boolean add = customer.addLoansAccount(account);

        if (add) {
            System.out.println("Quy khach da them tai khoan THANH CONG");
        } else {
            System.out.println("Them tai khoan KHONG thanh cong do stk da ton tai");
        }
    }


    /**
     * hàm kiểm tra tính không hợp lệ của stk
     *  @param stk : số tài khoản
     * @return :true/false
     */
    public static boolean validateAccount(String stk) {
        boolean isNumber = stk.chars().allMatch(Character::isDigit);
        return stk.length() != 6 || !isNumber;
    }


    /**
     * ham chuc nang 4: rút tiền
     */

    public static void withdrawMoney() {
        System.out.println();
        System.out.println();
        System.out.println("+----------+------------------------+----------+");
        System.out.println("|         CHON LOAI TAI KHOAN RUT TIEN         |");
        System.out.println("+----------+------------------------+----------+");
        System.out.println("1. SAVINGS");
        System.out.println("2. LOANS");
        System.out.println("0. Thoat");
        System.out.println("+----------+------------------------+----------+");
        System.out.println();
        System.out.println();

        int moduleNumber = -1;
        do {
            moduleNumber = inputModuleNumber();
        }while(moduleNumber < 0 || moduleNumber > 2);

        if(moduleNumber == 0){
            System.exit(0);
        }

        String stk = "";
        do {
            System.out.print("Nhap STK can rut tien: ");
            stk = sc.nextLine();
        } while (!isAccountNumberExisted(stk));

        boolean flag = false;
        double amount = 0;
        do {
            System.out.print("Nhap so tien can  rut: ");
            try {
                flag = false;
                amount = sc.nextDouble();
                sc.nextLine();
            } catch (Exception ex){
                flag = true;
                System.out.print("Vui long nhap lai so tien can rut: ");
            }
        }while (flag);

        if (moduleNumber ==1){
            SavingsAccount savingsAccount = (SavingsAccount) getAccountByAccountNumber(stk);
            boolean status = false;
            if (savingsAccount != null) {
                status = savingsAccount.withdraw(amount);
            }
            if(status){
                System.out.println("Rut tien THANH CONG");
                savingsAccount.log(amount);
            }else {
                System.out.println("Rut tien KHONG thanh cong");
            }
        }else {
            LoansAccount loansAccount = (LoansAccount) getAccountByAccountNumber(stk);
            boolean status = false;
            if (loansAccount != null) {
                status = loansAccount.withdraw(amount);
            }
            if(status){
                System.out.println("Rut tien THANH CONG");
                loansAccount.log(amount);
            }else {
                System.out.println("Rut tien KHONG thanh cong");
            }
        }
    }


    /**
     * hàm kiểm tra stk đã tồn tại chưa. Tồn tại => true, không tồn tại => false
     * @param stk : số tài khoản
     * @return true/false
     */
    public static boolean isAccountNumberExisted(String stk){
        DigitalCustomer digitalCustomer = activeBank.getCustomerById(CUSTOMER_ID);
        for(SavingsAccount savingsAccount : digitalCustomer.getSavingsAccounts()){
            if (savingsAccount.getAccountNumber().equals(stk)){
                return true;
            }
        }
        for(LoansAccount loansAccount : digitalCustomer.getLoansAccounts()){
            if (loansAccount.getAccountNumber().equals(stk)){
                return true;
            }
        }
        return false;
    }


    /**
     * hàm tìm thông tin tài khoản thông qua stk
     * @param stk : số tài khoản
     * @return : Account
     */
    public static Account getAccountByAccountNumber(String stk){
        DigitalCustomer digitalCustomer = activeBank.getCustomerById(CUSTOMER_ID);
        for(SavingsAccount savingsAccount : digitalCustomer.getSavingsAccounts()){
            if (savingsAccount.getAccountNumber().equals(stk)){
                return savingsAccount;
            }
        }
        for(LoansAccount loansAccount : digitalCustomer.getLoansAccounts()){
            if (loansAccount.getAccountNumber().equals(stk)){
                return loansAccount;
            }
        }
        return null;
    }


    /**
     * ham chuc nang 5: Lịch sử giao dịch
     */
    public static void getTransactionHistory(){

        DigitalCustomer digitalCustomer = activeBank.getCustomerById(CUSTOMER_ID);

        String customerType = "";
        if (digitalCustomer.isCustomerPremium()) {
            customerType = "Premium";
        } else {
            customerType = "Normal";
        }
        String totalBalance = digitalCustomer.formatCurrencyVN(digitalCustomer.getTotalAccountBalance());

        System.out.println();
        System.out.println("+----------+------------------------------+----------+");
        System.out.println("                  LICH SU GIAO DICH");
        System.out.println("+----------+------------------------------+----------+");
        System.out.println(digitalCustomer.getCustomerId() + "   |       " + digitalCustomer.getName() + " | " + customerType + " | " + totalBalance);

        int count = 1;
        List<Transaction> transactions = new ArrayList<>();

        for (SavingsAccount account : digitalCustomer.getSavingsAccounts()) {
            System.out.println(count + "    " + account.getAccountNumber()
                    + "    |     SAVINGS |           "
                    + digitalCustomer.formatCurrencyVN(account.getBalance()));
            count++;
            transactions.addAll(account.getTransactions());
        }

        for (LoansAccount account : digitalCustomer.getLoansAccounts()) {
            System.out.println(count + "    " + account.getAccountNumber()
                    + "    |       LOANS |           "
                    + digitalCustomer.formatCurrencyVN(account.getBalance()));
            count++;
            transactions.addAll(account.getTransactions());
        }

        for (Transaction transaction : transactions){
            System.out.println();
            System.out.println("[GD] " + transaction.getAccountNumber() +  "   |  "
                    + digitalCustomer.formatCurrencyVN(transaction.getAmount()) + "   |   "
                    + transaction.getTime());
        }
    }
}
