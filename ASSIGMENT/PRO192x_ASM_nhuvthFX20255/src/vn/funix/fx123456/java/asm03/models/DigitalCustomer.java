package vn.funix.fx123456.java.asm03.models;

import vn.funix.fx123456.java.asm02.models.Customer;

import java.util.ArrayList;
import java.util.List;

public class DigitalCustomer extends Customer {
    List<LoansAccount> loansAccounts = new ArrayList<>();
    List<SavingsAccount> savingsAccounts = new ArrayList<>();

    public List<LoansAccount> getLoansAccounts() {
        return loansAccounts;
    }

    public List<SavingsAccount> getSavingsAccounts() {
        return savingsAccounts;
    }

    /**
     * hàm thêm tài khoản mới nếu tài khoản đó chưa tồn tại
     * @param newAccount : thông tin tài khoản mới
     * @return : true/false
     */
    public boolean addSavingsAccount(SavingsAccount newAccount) {
        boolean flag = false;
        if (savingsAccounts != null) {
            for (SavingsAccount account : this.savingsAccounts) {
                if (account.getAccountNumber().equals(newAccount.getAccountNumber())) {
                    flag = true;
                    break;
                }
            }
            for (LoansAccount account : this.loansAccounts) {
                if (account.getAccountNumber().equals(newAccount.getAccountNumber())) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                this.savingsAccounts.add(newAccount);
                return true;
            }
        }
        return false;

    }


    /**
     * hàm thêm tài khoản mới nếu tài khoản đó chưa tồn tại
     * @param newAccount : thông tin tài khoản mới
     * @return : true/false
     */
    public boolean addLoansAccount(LoansAccount newAccount) {
        boolean flag = false;
        if (loansAccounts != null) {
            for (LoansAccount account : this.loansAccounts) {
                if (account.getAccountNumber().equals(newAccount.getAccountNumber())) {
                    flag = true;
                    break;
                }
            }
            for (SavingsAccount account : this.savingsAccounts) {
                if (account.getAccountNumber().equals(newAccount.getAccountNumber())) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                this.loansAccounts.add(newAccount);
                return true;
            }
        }
        return false;

    }


    /**
     * hiển thị thông tin khách hàng
     */
    @Override
    public void displayInformation() {
        String customerId = super.getCustomerId();

        String customerName = super.getName();

        String customerType = "";
        if (this.isCustomerPremium()) {
            customerType = "Premium";
        } else {
            customerType = "Normal";
        }
        String totalBalance = this.formatCurrencyVN(this.getTotalAccountBalance());

        System.out.println();
        System.out.println("+----------+------------------------------+----------+");
        System.out.println("                THONG TIN KHACH HANG");
        System.out.println("+----------+------------------------------+----------+");
        System.out.println(customerId + "   |       " + customerName + " | " + customerType + " | " + totalBalance);

        int count = 1;

        for (SavingsAccount account : this.savingsAccounts) {
            System.out.println(count + "    " + account.getAccountNumber() + "    |     SAVINGS |           "
                    + formatCurrencyVN(account.getBalance()));
            count++;
        }
        for (LoansAccount account : this.loansAccounts) {
            System.out.println(count + "    " + account.getAccountNumber() + "    |      LOANS  |           "
                    + formatCurrencyVN(account.getBalance()));
            count++;
        }
    }

    /**
     * hàm tính tổng số dư tất cả tài khoảng của khách hàng
     * @return : tổng tài khoản
     */
    public double getTotalAccountBalance() {
        double answer = 0;
        for (SavingsAccount account : this.savingsAccounts) {
            answer += account.getBalance();
        }
        for (LoansAccount account : this.loansAccounts) {
            answer += account.getBalance();
        }
        return answer;
    }

    /**
     * hàm rút tiền
     * @param accountNumber :stk
     * @param amount: số tiền cần rút
     */
    public void withdraw(String accountNumber, double amount){
        for(LoansAccount loansAccount : loansAccounts){
            if (loansAccount.getAccountNumber().equals(accountNumber)){
                loansAccount.withdraw(amount);
                return;
            }
        }
        for (SavingsAccount savingsAccount : savingsAccounts){
            if (savingsAccount.getAccountNumber().equals(accountNumber)){
                savingsAccount.withdraw(amount);
                return;
            }
        }

    }

    /**
     * hàm kiểm tra khách hàng có phải premium hay k
     * @return : true/false
     */
    @Override
    public boolean isCustomerPremium() {
        for (SavingsAccount account : this.savingsAccounts) {
            if (account.isAccountPremium()) {
                return true;
            }
        }

        // moi them do
        for (LoansAccount loansAccount : loansAccounts){
            if (loansAccount.isAccountPremium()){
                return true;
            }
        }
        return false;
    }
}
