package vn.funix.fx123456.java.asm03.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm02.models.Account;
import vn.funix.fx123456.java.asm03.models.DigitalCustomer;
import vn.funix.fx123456.java.asm03.models.LoansAccount;
import vn.funix.fx123456.java.asm03.models.SavingsAccount;

import static org.junit.Assert.*;

public class DigitalCustomerTest {

    @Test
    public void getTotalAccountBalance() {
        DigitalCustomer digitalCustomer = new DigitalCustomer();

        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("121212");
        savingsAccount.setBalance(5_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("123456");
        loansAccount.setBalance(10_000_000);

        digitalCustomer.addLoansAccount(loansAccount);

        double actual = digitalCustomer.getTotalAccountBalance();

        Assert.assertEquals(15_000_000, actual, 0);
    }

    @Test
    public void getTotalAccountBalance_WhenOnlySavingsAccount() {
        DigitalCustomer digitalCustomer = new DigitalCustomer();

        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("121212");
        savingsAccount.setBalance(5_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        double actual = digitalCustomer.getTotalAccountBalance();

        Assert.assertEquals(5_000_000, actual, 0);
    }


    @Test
    public void getTotalAccountBalance_WhenOnlyLoansAccount() {
        DigitalCustomer digitalCustomer = new DigitalCustomer();

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("123456");
        loansAccount.setBalance(10_000_000);

        digitalCustomer.addLoansAccount(loansAccount);

        double actual = digitalCustomer.getTotalAccountBalance();

        Assert.assertEquals(10_000_000, actual, 0);
    }

    @Test
    public void isCustomerPremium_WhenReturnTrue() {
        DigitalCustomer digitalCustomer = new DigitalCustomer();

        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("123412");
        savingsAccount.setBalance(12_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        boolean actual= digitalCustomer.isCustomerPremium();

        Assert.assertTrue(actual);
    }

    @Test
    public void isCustomerPremium_WhenReturnFalse() {
        DigitalCustomer digitalCustomer = new DigitalCustomer();

        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountNumber("123412");
        savingsAccount.setBalance(1_000_000);

        digitalCustomer.addSavingsAccount(savingsAccount);

        boolean actual= digitalCustomer.isCustomerPremium();

        Assert.assertFalse(actual);
    }
}