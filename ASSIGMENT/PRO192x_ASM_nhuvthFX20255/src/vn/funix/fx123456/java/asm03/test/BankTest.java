package vn.funix.fx123456.java.asm03.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm02.models.Bank;
import vn.funix.fx123456.java.asm02.models.Customer;

import static org.junit.Assert.*;

public class BankTest {

    @Test
    public void isCustomerExisted_WhenReturnTrue() throws Exception {
        Bank bank = new Bank();
        Customer customer = new Customer();
        customer.setCustomerId("089777777777");
        customer.setName("vu");

        bank.addCustomer(customer);

        boolean actual = bank.isCustomerExisted("089777777777");

        Assert.assertTrue(actual);
    }


    @Test
    public void isCustomerExisted_WhenReturnFalse() throws Exception {
        Bank bank = new Bank();
        Customer customer = new Customer();
        customer.setCustomerId("089777777777");
        customer.setName("vu");

        bank.addCustomer(customer);

        boolean actual = bank.isCustomerExisted("089777977777");

        Assert.assertFalse(actual);
    }
}