package vn.funix.fx123456.java.asm03.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm02.models.Account;

import static org.junit.Assert.*;

public class AccountTest {

    @Test
    public void isAccountPremium_WhenReturnTrue() {
        Account account = new Account();
        account.setAccountNumber("121212");
        account.setBalance(10_000_000);

        boolean actual = account.isAccountPremium();

        Assert.assertTrue(actual);
    }

    @Test
    public void isAccountPremium_WhenReturnFalse() {
        Account account = new Account();
        account.setAccountNumber("121212");
        account.setBalance(5_000_000);

        boolean actual = account.isAccountPremium();

        Assert.assertFalse(actual);
    }
}