package vn.funix.fx123456.java.asm03.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm03.models.DigitalBank;
import vn.funix.fx123456.java.asm03.models.DigitalCustomer;

import static org.junit.Assert.*;

public class DigitalBankTest {

    @Test
    public void getCustomerById_WhenSuccess() throws Exception {
        // Giả định có sẵn 1 khách hàng trong ngân hàng
        // customerId, name là kết quả mong đợi của test case này
        String customerId = "092186758743";
        String name = "Vu";
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer(customerId, name);

        // Gọi tới hàm cần test để lấy giá trị thực tế
        DigitalCustomer customer = digitalBank.getCustomerById(customerId);

        // So sánh kết quả thực tế và mong đợi có khớp với nhau hay không
        Assert.assertEquals(customerId, customer.getCustomerId());
        Assert.assertEquals(name, customer.getName());
    }

    @Test
    public void getCustomerById_WhenCustomerIdIsNull(){
        DigitalBank digitalBank = new DigitalBank();
        String customerId = "092186758743";

        // Gọi tới hàm cần test để lấy giá trị thực tế
        DigitalCustomer customer = digitalBank.getCustomerById(customerId);

        // So sánh kết quả thực tế và mong đợi có khớp với nhau hay không
        Assert.assertNull(customer);
    }
}