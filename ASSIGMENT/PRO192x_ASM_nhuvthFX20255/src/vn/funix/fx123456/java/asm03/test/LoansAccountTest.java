package vn.funix.fx123456.java.asm03.test;

import org.junit.Assert;
import org.junit.Test;
import vn.funix.fx123456.java.asm03.models.DigitalBank;
import vn.funix.fx123456.java.asm03.models.DigitalCustomer;
import vn.funix.fx123456.java.asm03.models.LoansAccount;

public class LoansAccountTest {

    @Test
    public void withdraw_WhenAccepted() throws Exception {
        double amount = 120_000;
        String accountNumber = "222222";
        double balance = 5_000_000;

        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089333333333", "Thien");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089333333333");

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber(accountNumber);
        loansAccount.setBalance(balance);

        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);

        boolean actual = loansAccount1.withdraw(amount);

        Assert.assertTrue(actual);
    }

    @Test
    public void withdraw_WhenNotAccept() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089444444444" , "Loi");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089444444444");

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("333333");
        loansAccount.setBalance(100_000);

        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);

        boolean actual = loansAccount1.withdraw(60_000);

        Assert.assertFalse(actual);
    }

    @Test
    public void getFee_WhenAccountPremium() throws Exception {

        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089111111111", "Vu");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089111111111");
        double balance = 10_000_000;
        String numberAccount = "111111";
        double amount = 3_000_000;

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber(numberAccount);
        loansAccount.setBalance(balance);
        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);
        double fee = loansAccount1.getFee(amount);

        double expectFee = amount * loansAccount.LOAN_ACCOUNT_WITHDRAW_PREMIUM_FEE;
        Assert.assertEquals(expectFee, fee, 0);
    }

    @Test
    public void getFee_WhenAccountNormal() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089111111111", "Vu");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089111111111");
        double balance = 9_000_000;
        String numberAccount = "111111";
        double amount = 3_000_000;

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber(numberAccount);
        loansAccount.setBalance(balance);
        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);
        double fee = loansAccount1.getFee(amount);

        double expectFee = amount * loansAccount.LOAN_ACCOUNT_WITHDRAW_FEE;
        Assert.assertEquals(expectFee, fee, 0);
    }




    @Test
    public void isAccepted_WhenIsAccountPremiumTrue() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089555555555", "Vo");
        DigitalCustomer  digitalCustomer = digitalBank.getCustomerById("089555555555");

        LoansAccount loansAccount =new LoansAccount();
        loansAccount.setAccountNumber("444444");
        loansAccount.setBalance(10_000_000);

        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);

        boolean actual = loansAccount1.isAccepted(1_000_000);

        Assert.assertTrue(actual);
    }


    @Test
    public void isAccepted_WhenIsAccountPremiumFalse() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089666666666", "Thi");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089666666666");

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("787878");
        loansAccount.setBalance(10_000_000);

        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);

        boolean actual = loansAccount1.isAccepted(100_000_000);

        Assert.assertFalse(actual);

    }


    @Test
    public void isAccepted_WhenIsNormalTrue() throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089666666666", "Huynh");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089666666666");

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("232323");
        loansAccount.setBalance(5_000_000);

        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);

        boolean actual = loansAccount1.isAccepted(2_000_000);

        Assert.assertTrue(actual);
    }


    @Test
    public void isAccepted_WhenIsNormalFalse () throws Exception {
        DigitalBank digitalBank = new DigitalBank();
        digitalBank.addCustomer("089666666668", "Nhu");
        DigitalCustomer digitalCustomer = digitalBank.getCustomerById("089666666668");

        LoansAccount loansAccount = new LoansAccount();
        loansAccount.setAccountNumber("234323");
        loansAccount.setBalance(5_000_000);

        digitalCustomer.addLoansAccount(loansAccount);

        LoansAccount loansAccount1 = digitalCustomer.getLoansAccounts().get(0);

        boolean actual = loansAccount1.isAccepted(6_000_000);

        Assert.assertFalse(actual);
    }
}