package vn.funix.fx123456.java.asm03.models;

import vn.funix.fx123456.java.asm02.models.Account;
import vn.funix.fx123456.java.asm02.models.Customer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SavingsAccount extends Account implements Withdraw, ReportService {
    //số tiền tối đa mà khách hàng có thể rút cho 1 lần giao dịch
    private final double SAVINGS_ACCOUNT_MAX_WITHDRAW = 5_000_000;
    private final double SAVINGS_ACCOUNT_MIN_WITHDRAW = 50_000;

    private List<Transaction> transactions = new ArrayList<>();

    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * hàm rút tiền
     * @param amount : số tiền cần rút
     * @return : true/false
     */
    @Override
    public boolean withdraw(double amount) {
        Transaction transaction = new Transaction();
        transaction.setAccountNumber(getAccountNumber());
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        transaction.setTime(LocalDateTime.now().format(dateTimeFormatter));
        transaction.setAmount(-amount);

        double newBalance = 0;
        if (isAccepted(amount)){
            newBalance = getBalance() - amount;
            setBalance(newBalance);
            transaction.setStatus(true);
            transactions.add(transaction);
            return true;
        }
        transaction.setStatus(false);
        transactions.add(transaction);
        return false;
    }

    /**
     * hàm kiểm tra tính hợp lệ của số tiền cần rút
     * @param amount : số tiền cần rút
     * @return: true/ false
     */
    @Override
    public boolean isAccepted(double amount) {
        double remainBalance = super.getBalance()-amount;
        if (isAccountPremium() && amount >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && remainBalance >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && amount % 10_000 == 0 ){
            return true;
        }else if (amount >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && amount <= SAVINGS_ACCOUNT_MAX_WITHDRAW
                && remainBalance >= SAVINGS_ACCOUNT_MIN_WITHDRAW
                && amount % 10_000 == 0) {
            return true;
        }
        return false;
    }

    /**
     * hàm in ra biên lai giao dịch
     * @param amount : số tiền cần rút
     */
    @Override
    public void log(double amount) {
        System.out.println("+----------+--------------------------+----------+");
        System.out.println("            BIEN LAI GIAO DICH SAVINGS");
        System.out.println();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        System.out.printf("  Ngay G/D:                 %s%n", LocalDateTime.now().format(dateTimeFormatter));
        System.out.printf("  ATM ID:                 %s%n", "DIGITAL-BANK-ATM 2023");
        System.out.printf("  SO TK:                                 %s%n", getAccountNumber());
        System.out.printf("  SO TIEN:                           %s%n", new Customer().formatCurrencyVN(amount));
        System.out.printf("  SO DU:                            %s%n", new Customer().formatCurrencyVN(getBalance()));
        System.out.println("  PHI + VAT:                                 0đ");
        System.out.println("+----------+--------------------------+----------+");
    }

}
