package vn.funix.fx123456.java.asm03.models;

import vn.funix.fx123456.java.asm02.models.Account;
import vn.funix.fx123456.java.asm02.models.Customer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class LoansAccount extends Account implements Withdraw, ReportService {
    //phí rút tiền cho tài khoản thường
    public final double LOAN_ACCOUNT_WITHDRAW_FEE = 0.05;

    //phí rút tiền tài khoản premium
    public final double LOAN_ACCOUNT_WITHDRAW_PREMIUM_FEE = 0.01;

    private List<Transaction> transactions = new ArrayList<>();
    //hạn mức tối đa
    public final double LOAN_ACCOUNT_MAX_BALANCE = 100_000_000;
    private final double LOAN_ACCOUNT_MIN_WITHDRAW = 50_000;

    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * hàm rút tiền
     * @param amount : số tiền cần rút
     * @return : true/ false
     */
    @Override
    public boolean withdraw(double amount) {
        Transaction transaction = new Transaction();
        transaction.setAccountNumber(getAccountNumber());
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        transaction.setTime(LocalDateTime.now().format(dateTimeFormatter));
        transaction.setAmount(amount);
        double newBalance = 0;
        if (isAccepted(amount)) {
            newBalance = getBalance() - amount - getFee(amount);

            setBalance(newBalance);
            transaction.setStatus(true);
            transactions.add(transaction);
            return true;
        }
        transaction.setStatus(false);
        transactions.add(transaction);
        return false;
    }

    /**
     * Tinh phi giao dich
     * @param amount: so tien can rut
     * @return tra ve phi giao dich tuong ung vơi tung loai tai khoan Normal/Premium
     */
    public double getFee(double amount){
        if (isAccountPremium()){
            return amount * LOAN_ACCOUNT_WITHDRAW_PREMIUM_FEE;
        }
        return amount * LOAN_ACCOUNT_WITHDRAW_FEE;
    }

    /**
     * hàm kiểm tra tính hợp lệ của số tiền cần rút
     * @param amount : số tiền cần rút
     * @return true/ false
     */
    @Override
    public boolean isAccepted(double amount) {
        if (isAccountPremium()) {
            double remainBalance = super.getBalance() - amount - amount * LOAN_ACCOUNT_WITHDRAW_PREMIUM_FEE;
            if (amount < LOAN_ACCOUNT_MAX_BALANCE && remainBalance >= LOAN_ACCOUNT_MIN_WITHDRAW) {
                return true;
            }
        } else {
            double remainBalance = super.getBalance() - amount - amount * LOAN_ACCOUNT_WITHDRAW_FEE;
            if (amount < LOAN_ACCOUNT_MAX_BALANCE && remainBalance >= LOAN_ACCOUNT_MIN_WITHDRAW) {
                return true;
            }
        }

        return false;
    }

    /**
     * hàm in ra biên lai giao dịch
     * @param amount: số tiền cần rút
     */
    @Override
    public void log(double amount) {
        System.out.println("+----------+--------------------------+----------+");
        System.out.println("            BIEN LAI GIAO DICH LOAN");
        System.out.println();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        System.out.printf("  Ngay G/D:                 %s%n", LocalDateTime.now().format(dateTimeFormatter));
        System.out.printf("  ATM ID:                 %s%n", "DIGITAL-BANK-ATM 2023");
        System.out.printf("  SO TK:                                 %s%n", getAccountNumber());
        System.out.printf("  SO TIEN:                          %s%n", new Customer().formatCurrencyVN(amount));
        System.out.printf("  SO DU:                            %s%n", new Customer().formatCurrencyVN(getBalance()));
        System.out.printf("  PHI + VAT:                           %s%n", new Customer().formatCurrencyVN(getFee(amount)));
        System.out.println("+----------+--------------------------+----------+");
    }
}
