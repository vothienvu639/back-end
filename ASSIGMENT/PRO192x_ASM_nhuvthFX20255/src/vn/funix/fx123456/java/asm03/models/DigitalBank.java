package vn.funix.fx123456.java.asm03.models;

import vn.funix.fx123456.java.asm02.models.Bank;

import java.util.ArrayList;
import java.util.List;

public class DigitalBank extends Bank {
    List<DigitalCustomer> digitalCustomers = new ArrayList<>();

    /**
     * hàm tìm thông tin khách hàng dựa vào số CCCD
     * @param customerId: số CCCD
     * @return: customer
     */
    public DigitalCustomer getCustomerById(String customerId) {
        for (DigitalCustomer customer : digitalCustomers){
            if (customer.getCustomerId().equals(customerId)){
                return customer;
            }
        }
        return null;
    }

    /**
     * hàm thêm khách hàng vào ngân hàng
     * @param customerId : số CCCD
     * @param name: tên khách hàng
     * @throws Exception: => Exception khi customerId k hợp lệ
     */
    public void addCustomer(String customerId, String name) throws Exception {
        DigitalCustomer digitalCustomer = new DigitalCustomer();
        digitalCustomer.setCustomerId(customerId);
        digitalCustomer.setName(name);
        digitalCustomers.add(digitalCustomer);
    }

    /**
     * hàm rút tiền
     * @param customerId : số CCCD
     * @param accountNumber : stk
     * @param amount: số tiền cần rút
     */
    public void withdraw(String customerId, String accountNumber, double amount){
        for (DigitalCustomer digitalCustomer : digitalCustomers){
            if (digitalCustomer.getCustomerId().equals(customerId)){
                digitalCustomer.withdraw(accountNumber, amount);
            }
        }
    }
}
