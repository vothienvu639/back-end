package vn.funix.fx123456.java.test;

import org.junit.Assert;
import org.junit.Test;

public class ShelfTest {
    @Test
    public void shelfCanAcceptAndReturnItem() {
        Shelf shelf = new Shelf();
        shelf.put("Orange");
        Assert.assertTrue(shelf.take("Orange"));
    }


    @Test
    public void shelfCanNotAcceptWhenItemNull() {
        Shelf shelf = new Shelf();
        shelf.put(null);
        Assert.assertFalse(shelf.take(null));
    }


    @Test
    public void shelfCanNotAcceptWhenItemEmpty() {
        Shelf shelf = new Shelf();
        shelf.put("");
        Assert.assertFalse(shelf.take(""));
    }


    @Test
    public void shelfItemTakenShelfOnce() {
        Shelf shelf = new Shelf();
        shelf.put("Orange");
        Assert.assertTrue(shelf.take("Orange"));
        Assert.assertFalse(shelf.take("Orange"));
    }


    @Test
    public void shelfDuplicateItemExist() {
        Shelf shelf = new Shelf();
        shelf.put("Orange");
        shelf.put("Orange");
        Assert.assertTrue(shelf.take("Orange"));
        Assert.assertTrue(shelf.take("Orange"));
    }
}
